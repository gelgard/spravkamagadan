//
//  AddOrganization.m
//  SpravkaMagadan
//
//  Created by gelgard on 27.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "AddOrganization.h"
#import "AFNetworking.h"

@interface AddOrganization ()

@end

@implementation AddOrganization
@synthesize nameAddOrg,deiatOrg, nameEditOrg, infoOrg, textOrg, adresOrg, yourPhoneOrg,textErr, supposeErr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if (IS_OS_7_OR_LATER)
    {
        self.textOrg.layer.borderColor = [[UIColor blackColor] CGColor];
        self.textOrg.layer.borderWidth = 1;
        
        
        self.textErr.layer.borderColor = [[UIColor blackColor] CGColor];
        self.textErr.layer.borderWidth = 1;
        
        
        self.supposeErr.layer.borderColor = [[UIColor blackColor] CGColor];
        self.supposeErr.layer.borderWidth = 1;
    }
    
    
       placeholderText = @"*Ваш телефон для проверки информации (не публикуется)";
    
    self.textOrg.delegate = self;
    self.textErr.delegate = self;
    self.supposeErr.delegate = self;
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
  
    if (IS_IPHONE5)
    {
        //scrollOrg.scrollEnabled = NO;
    }
    
    NSURL *websiteUrl = [NSURL URLWithString:aboutPath];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [aboutHTML setBackgroundColor:[UIColor clearColor]];
    [aboutHTML setOpaque:NO];
    [aboutHTML loadRequest:urlRequest];
    
    // Do any additional setup after loading the view.
}

-(void)onKeyboardShow:(NSNotification *)notification
{
    if (selPunkt == SendErrorView || selPunkt == SupposeBookView) {
        NSDictionary* keyboardInfo = [notification userInfo];
        NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
        CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
        
      
    }
}
-(void)onKeyboardHide:(NSNotification *)notification
{
    //keyboard will hide
    
    [buttonView setFrame:CGRectMake(0, self.view.bounds.size.height-buttonView.frame.size.height, buttonView.frame.size.width, buttonView.frame.size.height)];
    if (self.yourPhoneOrg.text.length > 0) return;
    [scrollViewPlacehoder setAlpha:1];
    [placeHolderLabel setFrame:CGRectMake(0, 0, 380, 30)];
    self.yourPhoneOrg.placeholder = @"";
    CGPoint bottomOffset = CGPointMake(0, 0);
    [scrollOrg setContentOffset:bottomOffset animated:YES];
    //[self.searchTex becomeFirstResponder];
    
    
}

-(void)scrollText:(id)parameter{
    
    placeHolderLabel.center = CGPointMake(placeHolderLabel.center.x - 1.2, placeHolderLabel.center.y);
    
    if (placeHolderLabel.center.x < -(placeHolderLabel.bounds.size.width/1.5)){
        placeHolderLabel.center = CGPointMake(320 + (placeHolderLabel.bounds.size.width/1/5), placeHolderLabel.center.y);
    }
    
    placeHolderLabelError.center = CGPointMake(placeHolderLabel.center.x - 1.2, placeHolderLabel.center.y);
    
    if (placeHolderLabelError.center.x < -(placeHolderLabel.bounds.size.width/1.5)){
        placeHolderLabelError.center = CGPointMake(320 + (placeHolderLabel.bounds.size.width/1/5), placeHolderLabel.center.y);
    }
    
    placeHolderLabelIssue.center = CGPointMake(placeHolderLabel.center.x - 1.2, placeHolderLabel.center.y);
    
    if (placeHolderLabelIssue.center.x < -(placeHolderLabel.bounds.size.width/1.5)){
        placeHolderLabelIssue.center = CGPointMake(320 + (placeHolderLabel.bounds.size.width/1/5), placeHolderLabel.center.y);
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
    myTimer = [NSTimer scheduledTimerWithTimeInterval:0.05f
                                               target: self
                                             selector:@selector(scrollText:)
                                             userInfo: nil
                                              repeats:YES];

        [scrollViewPlacehoderError setAlpha:1];

        [scrollViewPlacehoderIssue setAlpha:1];
    
    [[NSRunLoop mainRunLoop] addTimer:myTimer forMode:NSRunLoopCommonModes];
    
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark IBACtion
-(IBAction)clckOnPlaceholderScroll:(id)sender
{
    
    if (self.yourPhoneOrg.text.length >0) return;
    CGPoint bottomOffset = CGPointMake(0, scrollOrg.contentSize.height - scrollOrg.bounds.size.height);
    [scrollOrg setContentOffset:bottomOffset animated:YES];
   
    [scrollViewPlacehoder setAlpha:0];
    self.yourPhoneOrg.placeholder = placeholderText;
    [self.yourPhoneOrg becomeFirstResponder];

}

-(IBAction)clckOnPlaceholderError:(id)sender
{
    
    if (self.yourPhoneError.text.length >0) return;
//    CGPoint bottomOffset = CGPointMake(0, scrollOrg.contentSize.height - scrollOrg.bounds.size.height);
//    [scrollOrg setContentOffset:bottomOffset animated:YES];
    
    [scrollViewPlacehoderError setAlpha:0];
    self.yourPhoneError.placeholder = placeholderText;
    [self.yourPhoneError becomeFirstResponder];
    
}


-(IBAction)clckOnPlaceholderIssue:(id)sender
{
    
    if (self.yourPhoneIssue.text.length >0) return;
//    CGPoint bottomOffset = CGPointMake(0, scrollOrg.contentSize.height - scrollOrg.bounds.size.height);
//    [scrollOrg setContentOffset:bottomOffset animated:YES];
//    
    [scrollViewPlacehoderIssue setAlpha:0];
    self.yourPhoneIssue.placeholder = placeholderText;
    [self.yourPhoneIssue becomeFirstResponder];
    
}
-(IBAction)clickOnBigText:(id)sender
{
  
      
    
}
-(IBAction)searchGo:(id)sender
{
    
    if (self.yourPhoneError.text.length == 0) {
        [scrollViewPlacehoderError setAlpha:1];
        self.yourPhoneError.placeholder = @"";
    }
    
    
    if (self.yourPhoneIssue.text.length == 0) {
        [scrollViewPlacehoderIssue setAlpha:1];
        self.yourPhoneIssue.placeholder = @"";
    }
}
-(void)initingView:(MenuPunkts )punkts
{
    
   
    
    NSArray *viewsToRemove = [scrollOrg subviews];
    for (UIView *v in viewsToRemove) [v removeFromSuperview];
    
    topViewAdd.alpha = (punkts == AddOrganizationView);
    topViewEdit.alpha = (punkts == EditOrganizationView);
    
  

    selPunkt = punkts;
    
    [backToSprav setAlpha:!(punkts == AboutView)];
    [scrollOrg layoutIfNeeded];
    switch (punkts) {
        case AddOrganizationView:
            [self.menuTitle setText:@"ДОБАВИТЬ ОРГАНИЗАЦИЮ"];
            
            [scrollOrg setContentSize:CGSizeMake(addView.frame.size.width, addView.frame.size.height)];
            [scrollOrg addSubview:addView];
            NSLog(@"%f %f %f",self.view.frame.size.width,addView.frame.size.width,scrollOrg.frame.size.width );
            addView.center = scrollOrg.center;
            placeholderViewChange.alpha = 0;
            placeholderView.alpha = 1;
            
            [polos1 setAlpha:1];
            [self.adresOrg setAlpha:1];
            
            [scrollViewPlacehoder setFrame:CGRectMake(20, 330, 280, 30)];
            [self.yourPhoneOrg setFrame:CGRectMake(20, 330, 275, 30)];
            [polos2 setFrame:CGRectMake(20, 360, 280, 1)];
            
            break;
            
        case EditOrganizationView:
            [self.menuTitle setText:@"ИЗМЕНИТЬ ДАННЫЕ ОБ ОРГАНИЗАЦИИ"];
            [scrollOrg setContentSize:CGSizeMake(addView.frame.size.width, addView.frame.size.height)];
            [scrollOrg addSubview:addView];
            addView.center = scrollOrg.center;
            placeholderViewChange.alpha = 1;
            placeholderView.alpha = 0;
            
            [polos1 setAlpha:0];
            [self.adresOrg setAlpha:0];
            
            [self.yourPhoneOrg setFrame:adresOrg.frame];
            [scrollViewPlacehoder setFrame:adresOrg.frame];
            [polos2 setFrame:polos1.frame];

            break;
            
        case SendErrorView:
            [self.menuTitle setText:@"СООБЩИТЬ ОБ ОШИБКАХ В СПРАВОЧНИКЕ :("];
            [scrollOrg setContentSize:CGSizeMake(errorView.frame.size.width, errorView.frame.size.height)];
            [scrollOrg addSubview:errorView];
            errorView.center = scrollOrg.center;
            break;
            
        case SupposeBookView:
            [self.menuTitle setText:@"КНИГА ЖАЛОБ И ПРЕДЛОЖЕНИЙ"];
            [scrollOrg setContentSize:CGSizeMake(supposeView.frame.size.width, supposeView.frame.size.height)];
            [scrollOrg addSubview:supposeView];
            supposeView.center = scrollOrg.center;
            break;
            
        case MapView:
            [self.menuTitle setText:@""];
            [scrollOrg setContentSize:CGSizeMake(supposeView.frame.size.width, supposeView.frame.size.height)];
            [scrollOrg addSubview:supposeView];
            supposeView.center = scrollOrg.center;
            break;
            
        case AboutView:
            [self.menuTitle setText:@"О приложении"];
            aboutView.frame = CGRectMake(0, 0, scrollOrg.frame.size.width, scrollOrg.frame.size.height);
            //[scrollOrg setContentSize:CGSizeMake(aboutView.frame.size.width, aboutView.frame.size.height)];
            [scrollOrg addSubview:aboutView];
            supposeView.center = scrollOrg.center;
            break;
            
        default:
            break;
    }
    
    
    CGPoint bottomOffset = CGPointMake(0, 0);
    [scrollOrg setContentOffset:bottomOffset animated:YES];
}

-(IBAction)backToMain:(id)sender
{

    [myTimer invalidate];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"goToMainCateg" object:nil userInfo:nil];
}

#pragma mark TextView Delegate

-(void)textViewDidChange:(UITextView *)textView
{
    
    if (selPunkt == AddOrganizationView)
    {
        if (self.textOrg.text.length==0)
            placeholderView.alpha = 1;
        else
            placeholderView.alpha = 0;
    }
    
    
    if (selPunkt == EditOrganizationView)
    {
        if (self.textOrg.text.length==0)
            placeholderViewChange.alpha = 1;
        else
            placeholderViewChange.alpha = 0;
    }
    
    
    
    if (self.textErr.text.length==0)
        errorPlaceHolderView.alpha = 1;
    else
        errorPlaceHolderView.alpha = 0;

    if (self.supposeErr.text.length==0)
        supposePlaceHolderView.alpha = 1;
    else
        supposePlaceHolderView.alpha = 0;

    
}


-(IBAction)postData:(id)sender
{
     [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    NSString *urlString;
    NSDictionary *parameters = nil;
    switch (selPunkt) {
        case AddOrganizationView:
            urlString = [NSString stringWithFormat:@"%@%@",mainUrl,addOrg];
          
           parameters = @{@"name": self.nameAddOrg.text,@"info":self.deiatOrg.text ,@"number":self.textOrg.text ,@"address": self.adresOrg.text,@"sender": self.yourPhoneOrg.text};

            
            if (self.nameAddOrg.text.length == 0 || self.deiatOrg.text.length == 0 || self.adresOrg.text.length == 0 || self.yourPhoneOrg.text.length == 0 )
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Заполните все обязательные поля" delegate:nil cancelButtonTitle:@"OK"  otherButtonTitles:nil, nil];
                [alert show];
                return;
            }
            break;
            
        case EditOrganizationView:
            urlString = [NSString stringWithFormat:@"%@%@",mainUrl,editOrg];
            parameters = @{@"name": self.nameEditOrg.text,@"info":self.textOrg.text ,@"sender": self.yourPhoneOrg.text};
            if (self.nameEditOrg.text.length == 0 || self.yourPhoneOrg.text.length == 0  )
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Заполните все обязательные поля" delegate:nil cancelButtonTitle:@"OK"  otherButtonTitles:nil, nil];
                [alert show];
                return;
            }
            break;
            
        case SendErrorView:
          urlString = [NSString stringWithFormat:@"%@%@",mainUrl,errorAp];
           // parameters = @{@"info":self.textErr.text};
            parameters = [NSDictionary dictionaryWithObjects:@[self.textErr.text,self.yourPhoneError.text] forKeys:@[@"info",@"phone"]];
            if (self.textErr.text.length == 0  )
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Напишите пожалуйста сообщение!" delegate:nil cancelButtonTitle:@"OK"  otherButtonTitles:nil, nil];
                [alert show];
                return;
            }

            break;
            
        case SupposeBookView:
           urlString = [NSString stringWithFormat:@"%@%@",mainUrl,offers];
            //parameters = @{@"info":self.supposeErr.text };
            parameters = [NSDictionary dictionaryWithObjects:@[self.supposeErr.text,self.yourPhoneIssue.text] forKeys:@[@"info",@"phone"]];
            
            if (self.supposeErr.text.length == 0  )
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Напишите пожалуйста сообщение!" delegate:nil cancelButtonTitle:@"OK"  otherButtonTitles:nil, nil];
                [alert show];
                return;
            }
            break;
        default:
            break;
    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        self.nameAddOrg.text =@"";
        self.nameEditOrg.text = @"";
        self.deiatOrg.text = @"";
        self.adresOrg.text = @"";
        self.yourPhoneOrg.text = @"";
    self.supposeErr.text = @"";
        self.textOrg.text = @"";
        self.textErr.text = @"";
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Ваше сообщение отправленно!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
    }];

}

-(IBAction)hideKeyboardAll:(id)sender
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}
@end
