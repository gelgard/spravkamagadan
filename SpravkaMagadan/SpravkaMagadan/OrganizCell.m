//
//  OrganizCell.m
//  SpravkaMagadan
//
//  Created by gelgard on 27.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "OrganizCell.h"

@implementation OrganizCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)animatedShow
{
    [self.categName setFrame:CGRectMake(self.contentView.frame.size.width, self.categName.frame.origin.y, self.categName.frame.size.width, self.categName.frame.size.height)];
    [self.categName setAlpha:1];
    [self.adresName setFrame:CGRectMake(self.contentView.frame.size.width, self.adresName.frame.origin.y, self.adresName.frame.size.width, self.adresName.frame.size.height)];
    [self.adresName setAlpha:1];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:0.3]; //or lower than 0.1
    [self.categName setFrame:CGRectMake(20, self.categName.frame.origin.y, self.categName.frame.size.width, self.categName.frame.size.height)];
    [self.adresName setFrame:CGRectMake(20, self.adresName.frame.origin.y, self.adresName.frame.size.width, self.adresName.frame.size.height)];
    [UIView commitAnimations];
}


@end
