#import "MyCLController.h"

@implementation MyCLController

@synthesize locationManager;
@synthesize delegate;
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


- (id) init {
    self = [super init];
    if (self != nil) {
        self.locationManager = [[CLLocationManager alloc] init] ;
       
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        self.locationManager.delegate = self; // send loc updates to myself
        if(IS_OS_8_OR_LATER) {
            [self.locationManager requestAlwaysAuthorization];
        }
        
        [self.locationManager startUpdatingLocation];
    }
    return self;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
//    NSLog(@"%f %f %f %f", newLocation.coordinate.latitude,newLocation.coordinate.longitude, oldLocation.coordinate.latitude,oldLocation.coordinate.longitude);
    CLLocationDistance distance = [newLocation distanceFromLocation:oldLocation];
    
//    NSLog(@"distance %f",distance);
    
    
   
 
        
        [self.delegate locationUpdate:newLocation];
    
    
}


- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"%@", [error localizedDescription]);
    [self.delegate locationError:error];
}



@end
