//
//  CategController_0.m
//  SpravkaMagadan
//
//  Created by gelgard on 26.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "CategController_0.h"

@interface CategController_0 ()

@end

@implementation CategController_0

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(IBAction)openCategory:(id)sender
{
    NSString *title;
    
    switch (((UIButton *)sender).tag) {
        case 0:
            title = @"ТАКСИ";
            break;
        case 1:
            title = @"Бары|Кафе Рестораны";
            break;
            
        case 2:
            title = @"Солярии";
            break;
        case 3:
            title = @"Салоны красоты";
            break;
        case 4:
            title = @"Сауны и Бани";
            break;
        case 5:
            title = @"Аптеки";
            break;
        case 6:
            title = @"Окна|Двери";
            break;
        case 7:
            title = @"Натяжные потолки";
            break;
        case 8:
            title = @"Экстренные службы";
            break;
        default:
            break;
    }
    
    NSDictionary *titleInfo = [[NSDictionary alloc] initWithObjectsAndKeys:title,@"title", [NSString stringWithFormat:@"%@",@"111"],@"placeholder",nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"categorySelected" object:titleInfo userInfo:nil];
}
@end
