//
//  SelectedCategory.h
//  SpravkaMagadan
//
//  Created by gelgard on 27.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopPanel.h"
#import "CategController_2.h"



@interface SelectedCategory : UIViewController
{
    TopPanel *topPanel;
    
    IBOutlet UIView *proizvodstvaView;
    CategController_2 *proivodstvaController;
    NSTimer* myTimer;
    NSTimer* myTimer2;
    IBOutlet UIScrollView *scrollViewPlacehoder;
    BOOL notUpdate;
}

-(IBAction)backBtn:(id)sender;

@property (nonatomic, strong) IBOutlet UILabel *categTitle;
@property (nonatomic, strong) IBOutlet UILabel *placeHolderLabel;
@property (nonatomic, strong) IBOutlet UITextField *searchTex;
@property (nonatomic, strong) NSString *placeholderText;

-(IBAction)clckOnPlaceholderScroll:(id)sender;
-(IBAction)searchGo:(id)sender;
-(IBAction)editStart:(id)sender;
-(IBAction)cancelClick:(id)sender;
@end
