//
//  CategCell.m
//  SpravkaMagadan
//
//  Created by gelgard on 27.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "CategCell.h"

@implementation CategCell

@synthesize categName;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)animatedShow
{
    [categName setFrame:CGRectMake(self.contentView.frame.size.width, categName.frame.origin.y, categName.frame.size.width, categName.frame.size.height)];
    [categName setAlpha:1];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:0.3]; //or lower than 0.1
    [categName setFrame:CGRectMake(20, categName.frame.origin.y, categName.frame.size.width, categName.frame.size.height)];
    [UIView commitAnimations];
}

@end
