//
//  BaseInfo.h
//  SpravkaMagadan
//
//  Created by gelgard on 06.05.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BaseInfo : NSManagedObject

@property (nonatomic, retain) NSNumber * version;

@end
