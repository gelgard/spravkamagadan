//
//  AddOrganization.h
//  SpravkaMagadan
//
//  Created by gelgard on 27.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PanelDelegate.h"

#import "GCPlaceholderTextView.h"


@interface AddOrganization : UIViewController <UITextViewDelegate>
{
    IBOutlet UIView *addView;
    IBOutlet UIView *errorView;
    
    IBOutlet UIView *supposeView;
    IBOutlet UIView *aboutView;
    
    IBOutlet UIScrollView* scrollOrg;
    IBOutlet UITextView *bigText;
    IBOutlet UIView *errorPlaceHolderView;
    IBOutlet UIView *supposePlaceHolderView;
    
    
    IBOutlet UIView *placeholderView;
    IBOutlet UIView *placeholderViewChange;
    
    IBOutlet UIView *buttonView;
    
    IBOutlet UIView *topViewAdd;
    IBOutlet UIView *topViewEdit;
    
    IBOutlet UIButton *backToSprav;
    IBOutlet UIWebView *aboutHTML;
    
    
    
    
    
    
    IBOutlet UIScrollView *scrollViewPlacehoder;
    IBOutlet UIScrollView *scrollViewPlacehoderIssue;
    IBOutlet UIScrollView *scrollViewPlacehoderError;
    IBOutlet UITextField *placeHolderLabel;
    IBOutlet UITextField *placeHolderLabelError;
    IBOutlet UITextField *placeHolderLabelIssue;
    IBOutlet UITextField *searchTex;
    
    
    
    NSString *placeholderText;
    
    NSTimer* myTimer;
    
    MenuPunkts selPunkt;
    
    
    IBOutlet UIView *polos1;
    IBOutlet UIView *polos2;
}


@property (nonatomic, assign) id<PanelDelegate> delegate;
@property (nonatomic, strong) IBOutlet UILabel *menuTitle;


@property (nonatomic, strong) IBOutlet UITextField *nameAddOrg;
@property (nonatomic, strong) IBOutlet UITextField *deiatOrg;
@property (nonatomic, strong) IBOutlet UITextField *nameEditOrg;
@property (nonatomic, strong) IBOutlet UITextField *infoOrg;
@property (nonatomic, strong) IBOutlet UITextView *textOrg;
@property (nonatomic, strong) IBOutlet UITextField *adresOrg;
@property (nonatomic, strong) IBOutlet UITextField *yourPhoneOrg;
@property (nonatomic, strong) IBOutlet UITextField *yourPhoneError;
@property (nonatomic, strong) IBOutlet UITextField *yourPhoneIssue;
@property (nonatomic, strong) IBOutlet UITextView *textErr;
@property (nonatomic, strong) IBOutlet UITextView *supposeErr;

-(IBAction)clckOnPlaceholderScroll:(id)sender;
-(IBAction)clckOnPlaceholderError:(id)sender;
-(IBAction)clckOnPlaceholderIssue:(id)sender;
-(IBAction)searchGo:(id)sender;
-(IBAction)clickOnBigText:(id)sender;
-(void)initingView:(MenuPunkts )punkts;

-(IBAction)backToMain:(id)sender;

-(IBAction)postData:(id)sender;

-(IBAction)hideKeyboardAll:(id)sender;

@end
