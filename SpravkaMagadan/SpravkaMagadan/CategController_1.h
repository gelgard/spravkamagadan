//
//  CategController_1.h
//  SpravkaMagadan
//
//  Created by gelgard on 26.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategCell.h"
#import "BaseManager.h"

@interface CategController_1 : UIViewController
{
    IBOutlet CategCell *categCell;
    IBOutlet UITableView *categTabl;
    BOOL visibleCell;
    int countCell;
    BaseManager* baseManager;
}
-(void)animShow;
-(void)refreshData;
-(void)updateFetchWithFilter:(NSString *)filter;

@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;

@end
