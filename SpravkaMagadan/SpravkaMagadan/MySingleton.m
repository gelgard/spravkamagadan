//
//  MySingleton.m
//  offM
//
//  Created by apple on 02.09.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "MySingleton.h"







@implementation MySingleton

static MySingleton* _sharedMySingleton = nil;



+(MySingleton*)sharedMySingleton
{
	@synchronized([MySingleton class])
	{
		if (!_sharedMySingleton)
			_sharedMySingleton = [[MySingleton alloc]init];

        
		return _sharedMySingleton;
	}
    
	return nil;
}

+(id)alloc
{
	@synchronized([MySingleton class])
	{
		NSAssert(_sharedMySingleton == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedMySingleton = [super alloc];
		return _sharedMySingleton;
	}
    
	return nil;
}

-(id)init {
	self = [super init];
	if (self != nil) {
     
        locationController = [[MyCLController alloc] init];
        locationController.delegate = self;
        
        
        
        currentLocation = [[CLLocation alloc] init];
        currentLocation = nil;
		// initialize stuff here
        
    }
    
	return self;
}


-(NSString *)getSystemVersion
{
    
    if (IS_OS_7_OR_LATER) {
        return [NSString stringWithFormat:@"%@",@"7"];
    }
    
    if (IS_OS_6_OR_LATER) {
        return [NSString stringWithFormat:@"%@",@"6"];
    }
    
    return @"";
}
-(CLLocation *)getCurrentLocation
{
    return currentLocation;
}

#pragma mark Location Delegat

- (void)locationUpdate:(CLLocation *)location
{
    
    
    
    currentLocation = location;
    
}
- (void)locationError:(NSError *)error
{
    NSLog(@"ERROR");
}


- (double)heightFromString:(NSString*)text withFont:(UIFont*)font constraintToWidth:(double)widthL
{
    CGRect rect;
    
    float iosVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (iosVersion >= 7.0) {
        rect = [text boundingRectWithSize:CGSizeMake(widthL, 1000) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font} context:nil];
    }
    else {
        CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(widthL, 1000) lineBreakMode:NSLineBreakByWordWrapping];
        rect = CGRectMake(0, 0, size.width, size.height);
    }
    NSLog(@"%@: W: %.f, H: %.f", self, rect.size.width, rect.size.height);
    return rect.size.height;
}


@end