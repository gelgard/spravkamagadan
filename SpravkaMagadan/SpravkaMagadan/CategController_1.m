//
//  CategController_1.m
//  SpravkaMagadan
//
//  Created by gelgard on 26.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "CategController_1.h"
#import "Organization.h"


@interface CategController_1 ()

@end

@implementation CategController_1
@synthesize fetchedResultsController = _fetchedResultsController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    visibleCell = NO;
   
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateAnimated:) name:@"updateAnimated" object:nil];
    
     baseManager = [[BaseManager alloc] init];
    NSError *error;
	if (![[baseManager fetchedResultsControllerCategory:@"" full:YES] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
        
	}
    _fetchedResultsController = [baseManager fetchedResultsControllerCategory:@"" full:YES];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateFetch:) name:@"updateFetch" object:nil];
    // Do any additional setup after loading the view.
}

-(void)updateFetch:(NSNotification *)notification
{
    
    _fetchedResultsController = [baseManager fetchedResultsControllerCategory:@"" full:YES];
    [categTabl reloadData];
}

-(void)updateFetchWithFilter:(NSString *)filter
{
    NSLog(@" FETCH %@",filter);
    _fetchedResultsController = [baseManager fetchedResultsControllerCategory:filter full:NO];
    
    [categTabl reloadData];
    [self refreshData];
}
-(void)updateAnimated:(NSNotification *)notification
{
    
    [self animShow];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)animCurrentCell:(int)cellIndex
{
    if (countCell==0) {
        return;
    }
    UITableView *tableView = categTabl; // Or however you get your table view
   NSIndexPath *path = [[tableView indexPathsForVisibleRows] objectAtIndex:cellIndex];
    __block int newIndex = cellIndex+1;

    
    NSTimeInterval delayInSeconds = 0.08;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [((CategCell *)[tableView cellForRowAtIndexPath:path]) animatedShow] ;
        if (newIndex < [[tableView indexPathsForVisibleRows] count])
            [self animCurrentCell:newIndex];
        
    });
    
    
}

-(void)animShow
{

          visibleCell = NO;
    [categTabl reloadData];
  
    
    NSTimeInterval delayInSeconds = 0.08;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

        
    [self refreshData];

    });
    
}
-(void)refreshData
{
  
    if (!visibleCell) {
        visibleCell = YES;
        
        [self animCurrentCell:0];
    }
    
}


#pragma mark - Table view data source








- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return countCell;    // Return the number of rows in the section.
    id  sectionInfo =
    [[_fetchedResultsController sections] objectAtIndex:section];
    
    countCell = (int)[sectionInfo numberOfObjects];
    return countCell;
}


- (void)configureCell:(CategCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
  
    if (visibleCell) {
        [cell.categName setAlpha:1];
        
    } else
        [cell.categName setAlpha:0];
    
    
    NSManagedObject *org = [_fetchedResultsController objectAtIndexPath:indexPath];
    

    [cell.categName setText:[org valueForKey:@"categ"]];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"CategCell_%@",[[MySingleton sharedMySingleton] getSystemVersion]];
    
   
    
    CategCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        
        [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"CategCell_%@",[[MySingleton sharedMySingleton] getSystemVersion]] owner:self options:nil];
		cell = categCell;
    }
    
    [self configureCell:cell atIndexPath:indexPath];

    
    return cell;

}

#pragma mark - Table view delegate

//- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"account_cell_background_guide_selected.png"]];
//    UILabel *titleLabel = (UILabel*)[[[self.tableView cellForRowAtIndexPath:indexPath] contentView] viewWithTag:10];
//    
//    titleLabel.textColor = [UIColor whiteColor];
//    titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
//    return indexPath;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategCell *cell = (CategCell *)[tableView cellForRowAtIndexPath:indexPath];
NSDictionary *titleInfo = [[NSDictionary alloc] initWithObjectsAndKeys:cell.categName.text,@"title", [NSString stringWithFormat:@"%@",@"111"],@"placeholder",nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"categorySelected" object:titleInfo userInfo:nil];
    
}


#pragma mark NSFetchRequest

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    NSLog(@"1");
    [categTabl beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = categTabl;
    NSLog(@"2");
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(CategCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    NSLog(@"3");
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [categTabl insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [categTabl deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    NSLog(@"4");
    [categTabl endUpdates];
}


@end
