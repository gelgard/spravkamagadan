#import "MainViewController.h"
#import "GreenViewController.h"
#import "BlueViewController.h"
#import "RedViewController.h"
#import "NavigationViewController.h"
#import "PanelDelegate.h"
#import "TopPanel.h"
#import "SelectedCategory.h"
#import "AddOrganization.h"
#import "MapController.h"
#import "ServerCommunicaion.h"






#import "CategController.h"
#import <QuartzCore/QuartzCore.h>

#define CORNER_RADIUS 4
#define SLIDE_TIMING .25
#define PANEL_WIDTH 28

@interface MainViewController () <PanelDelegate,TopPanelDelegate, UIGestureRecognizerDelegate> {
    // This view controller doesn't have a UI.
    // activeViewController.view is what the user sees
    UIViewController *activeViewController;
}

@property (nonatomic, strong) GreenViewController *greenViewController;
@property (nonatomic, strong) BlueViewController *blueViewController;
@property (nonatomic, strong) RedViewController *redViewController;
@property (nonatomic, strong) NavigationViewController *navigationViewController;

@property (nonatomic, strong) CategController *categController;
@property (nonatomic, strong) SelectedCategory *selectedCategory;
@property (nonatomic,strong) AddOrganization *addOrganization;
@property (nonatomic,strong) MapController *mapView;
@property (nonatomic, assign) BOOL showPanel;
@property (nonatomic, assign) BOOL panelMovedRight;
@property (nonatomic, strong)  TopPanel *topPanel;
@end

@implementation MainViewController
@synthesize downloadView;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    
    baseManager = [[BaseManager alloc] init];
    baseManager.delegateBase = self;
    
    downloadView.dynamic = YES;
    downloadView.blurEnabled = IS_OS_7_OR_LATER;

    downloadViewLabel.layer.cornerRadius = 10;
    downloadViewLabel.layer.masksToBounds = YES;
    
//    if (IS_OS_7_OR_LATER) {
//        FXBlurView *blurView = [[FXBlurView alloc] initWithFrame:downloadView.frame];
//        [blurView setDynamic:YES];
//        [downloadView addSubview:blurView];
//        [downloadView bringSubviewToFront:downloadViewLabel];
//    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(goToMainCateg:) name:@"goToMainCateg" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openMenuPunkt:) name:@"openMenuPunkt" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBase:) name:@"updateBase" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openMap:) name:@"openMap" object:nil];
    // Do any additional setup after loading the view.
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
//    if (![baseManager isDBEmpty]) {
//        [self beginUpdate];
//    } else
//        
//    {
        [ServerCommunicaion downloadAndUnZip:VersionFile downloadedFilePath:^(NSString *fileDownloadedName) {
            
            
            
            if (fileDownloadedName) {
                NSString *myText = [NSString stringWithContentsOfFile:fileDownloadedName usedEncoding:0 error:nil];
                if (myText) {
                    NSLog(@"file path %@",myText);
                     currentVersion = (int)[myText integerValue];
                    if ([baseManager getDatabaseVersion]<currentVersion) {
                        
                        if ([baseManager getDatabaseVersion]==0)
                        {
                            [self beginUpdate];
                        }
                            else
                        if ([self isNewVersionExist]) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Новая база данных доступна." message:@"Обновить базу? (Рекомендуется)" delegate:self cancelButtonTitle:@"НЕТ" otherButtonTitles:@"ОБНОВИТЬ", nil];
                            
                            [alert show];
                        }
                    }
                    
                }  
            }
            
        }];

//    }
    
}


#pragma mark GetUpdate

-(BOOL)isNewVersionExist
{
    
        return YES;
}

-(void)beginUpdate
{

    [downloadLabel setText:@"Подождите пожалуйста.\nИдет загрузка базы данных ..."];
    [downloadView layoutIfNeeded];
    [self.view layoutIfNeeded];
    downloadView.frame = self.view.frame;
    [self.view addSubview:downloadView];
    
     baseManager.currentType = BaseFile;
    [ServerCommunicaion downloadAndUnZip:BaseFile downloadedFilePath:^(NSString *fileDownloadedName) {
               // dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
       // dispatch_async(q, ^{
        
        [self UpdateLabel];
        [baseManager saveVersion:currentVersion];
       
        [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        [baseManager parseCSV:fileDownloadedName];
       // });
       
    }];
}

-(void)UpdateLabel
{
   
        [downloadLabel setText:@"Идет обработка базы данных."];
    
}

#pragma mark Observers

-(void)openMap: (NSNotification*)aNotification
{
    
    
}

-(void)goToMainCateg: (NSNotification*)aNotification
{
    
    
   [self movePanelRight];
    
    goToRoot = YES;
    
}

-(void)updateBase: (NSNotification*)aNotification
{
    [self movePanelToOriginalPosition];
    [self beginUpdate];
}
-(void)openMenuPunkt: (NSNotification*)aNotification
{
   
    
    
    
    if (activeViewController != nil) {
        [activeViewController.view removeFromSuperview];
        activeViewController = nil;
    }
    
    
    if ([[aNotification.object objectForKey:@"btntag"] integerValue] != 5) {
        activeViewController =  _addOrganization;
       
    } else
    {
        
        
         activeViewController =  _mapView;
       
        
    }
     activeViewController.view.frame = CGRectMake(self.view.frame.size.width - PANEL_WIDTH, 0, self.view.frame.size.width, self.view.frame.size.height);
   
    
    [self.view addSubview:activeViewController.view];
    [self addChildViewController:activeViewController];
    
    [activeViewController didMoveToParentViewController:self];
    
    [self showActiveViewWithShadow:YES withOffset:-2];
    
    
   // NSLog(@"I %i",[[aNotification.object objectForKey:@"btntag"] integerValue]);
    switch ([[aNotification.object objectForKey:@"btntag"] integerValue]) {
        case 0:
            [_addOrganization initingView:AddOrganizationView];
            break;
        case 1:
            [_addOrganization initingView:EditOrganizationView];
            break;
        case 2:
            [_addOrganization initingView:SendErrorView];
            break;
            
        case 3:
            [_addOrganization initingView:SupposeBookView];
            break;
            
            
        case 5:
            [_addOrganization initingView:SupposeBookView];
            break;
            
        case 6:
            [_addOrganization initingView:AboutView];
            break;
        default:
            break;
    }
    
    [self movePanelToOriginalPosition];
   
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.5;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//    [self.view.layer addAnimation:transition forKey:nil];
//    
//    
//    [self.view addSubview:_addOrganization.view];
//   
   
    if ([activeViewController isKindOfClass:[MapController class]]) {
//        if ([aNotification.object objectForKey:@"latitude"] != nil  && [aNotification.object objectForKey:@"longitude"] != nil) {
           // NSLog(@"- %d",[aNotification.object objectForKey:@"latitude"] );
            ((MapController*)activeViewController).latitude = [[aNotification.object objectForKey:@"latitude"] doubleValue];
            ((MapController*)activeViewController).longitude = [[aNotification.object objectForKey:@"longitude"] doubleValue];
            [((MapController*)activeViewController).selectTitle setText:[aNotification.object objectForKey:@"adress"]];
        ((MapController*)activeViewController).FromInfo = NO;
            [(MapController*)activeViewController loadEmbedMap];
   //     }
    }
    
}

-(void)onKeyboardHide:(NSNotification *)notification
{
    [self movePanelToOriginalPosition];
}



#pragma mark -
#pragma mark Custom view methods

- (void)setupView {
    // When the app is launched we'll start by showing the red view
     NSLog(@"SV %@",[[MySingleton sharedMySingleton] getSystemVersion]);
    _categController = [[CategController alloc] initWithNibName:[NSString stringWithFormat:@"CategView_%@",[[MySingleton sharedMySingleton] getSystemVersion]] bundle:nil];
    _categController.delegate = self;
    _categController.topDelegate = self;
    [self.view addSubview:_categController.view];
    
//    NSString *nibName = [NSString stringWithFormat:@"%@_%@",@"TopPanel",[[MySingleton sharedMySingleton] getSystemVersion]  ]  ;
    
    
//    self.topPanel= [[TopPanel alloc] initWithNibName:nibName bundle:nil];
//    self.topPanel.delegate = self;
//    [_categController.view addSubview:self.topPanel.view];
//    [self.topPanel.view setFrame:CGRectMake(0, 0, self.topPanel.view.frame.size.width, self.topPanel.view.frame.size.height)];
//    
    [self addChildViewController:_categController];
    [_categController didMoveToParentViewController:self];
    activeViewController = _categController;
    
    NSString *nibNameOrg = [NSString stringWithFormat:@"%@_%@",@"AddOrganizationView",[[MySingleton sharedMySingleton] getSystemVersion]  ]  ;
    _addOrganization = [[AddOrganization alloc] initWithNibName:nibNameOrg bundle:nil];
    
    _mapView = [[MapController alloc] init];

    [self setupGestures];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(categorySelected:) name:@"categorySelected" object:nil];
    
    [self movePanelToOriginalPosition];
    // Do any additional setup after loading the view.
}

#pragma mark OBSERVER

-(void)categorySelected: (NSNotification*)aNotification
{
   
     self.selectedCategory = [[SelectedCategory alloc]  initWithNibName:[NSString stringWithFormat:@"SelectedCategoryView_%@",[[MySingleton sharedMySingleton] getSystemVersion]] bundle:nil];
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.5;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//    [self.view.layer addAnimation:transition forKey:nil];
    self.selectedCategory.view.frame = self.view.frame;
    NSLog(@" ct %@",[aNotification.object objectForKey:@"title"]);
    [self.view addSubview:self.selectedCategory.view];
    self.selectedCategory.placeholderText = [NSString stringWithFormat:@"Поиск организации по категории: %@",[aNotification.object objectForKey:@"title"]];
    [self.selectedCategory.placeHolderLabel setText:[NSString stringWithFormat:@"Поиск организации по категории: %@",[aNotification.object objectForKey:@"title"]]];

    self.selectedCategory.searchTex.placeholder = @"";
    [self.selectedCategory.categTitle setText:[aNotification.object objectForKey:@"title"]];
    
    
    
    NSLog(@"SELECTED %@",[aNotification.object objectForKey:@"title"]);
}

#pragma mark Slide Menu Actions

- (void)resetMainView {
    if (_navigationViewController != nil) {
        [_navigationViewController.view removeFromSuperview];
        _navigationViewController = nil;
    }
    
    [self showActiveViewWithShadow:NO withOffset:0];
}

// This is where the NavigationViewController eventually ends up via its delegate
- (void)showActiveViewWithName:(NSString *)viewName {
    if ([viewName isEqualToString:@"GreenView"]) {
        _greenViewController = [[GreenViewController alloc] initWithNibName:viewName bundle:nil];
        _greenViewController.delegate = self;
        activeViewController = _greenViewController;
    }
    else if ([viewName isEqualToString:@"BlueView"]) {
        _blueViewController = [[BlueViewController alloc] initWithNibName:viewName bundle:nil];
        _blueViewController.delegate = self;
        activeViewController = _blueViewController;
    }
    else if ([viewName isEqualToString:@"RedView"]) {
        _redViewController = [[RedViewController alloc] initWithNibName:viewName bundle:nil];
        _redViewController.delegate = self;
        activeViewController = _redViewController;
    } else if ([viewName isEqualToString:@"AddOrganizationView0"]) {
        
        _addOrganization = [[AddOrganization alloc] initWithNibName:[NSString stringWithFormat:@"AddOrganizationView_%@",[[MySingleton sharedMySingleton] getSystemVersion]] bundle:nil];
        _addOrganization.delegate = self;
        [_addOrganization initingView:AddOrganizationView];
        activeViewController = _addOrganization;
    }
   
    
    activeViewController.view.frame = CGRectMake(self.view.frame.size.width - PANEL_WIDTH, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self.view addSubview:activeViewController.view];
    [self addChildViewController:activeViewController];
    
    [activeViewController didMoveToParentViewController:self];
    
    [self showActiveViewWithShadow:YES withOffset:-2];
}

- (void)showActiveViewWithShadow:(BOOL)value withOffset:(double)offset {
    if (value) {
        [activeViewController.view.layer setCornerRadius:CORNER_RADIUS];
        [activeViewController.view.layer setShadowColor:[UIColor blackColor].CGColor];
        [activeViewController.view.layer setShadowOpacity:0.8];
        [activeViewController.view.layer setShadowOffset:CGSizeMake(offset, offset)];
    }
    else {
        [activeViewController.view.layer setCornerRadius:0.0f];
        [activeViewController.view.layer setShadowOffset:CGSizeMake(offset, offset)];
    }
}

- (UIView *)getNavigationView {
    if (_navigationViewController == nil) {
        NSLog(@"SV %@",[[MySingleton sharedMySingleton] getSystemVersion]);
        _navigationViewController = [[NavigationViewController alloc] initWithNibName:[NSString stringWithFormat:@"NavigationView_%@",[[MySingleton sharedMySingleton] getSystemVersion]] bundle:nil];
        _navigationViewController.delegate = self;
        [self.view addSubview:_navigationViewController.view];
        [self addChildViewController:_navigationViewController];
        [_navigationViewController didMoveToParentViewController:self];
        _navigationViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    
    [self showActiveViewWithShadow:YES withOffset:-2];
    
    return _navigationViewController.view;
}

-(void)setupGestures {
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movePanel:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [panRecognizer setDelegate:self];
    
    [self.view addGestureRecognizer:panRecognizer];
}

#pragma mark -
#pragma mark PanelDelegate methods
-(void)openRoot
{
    if (activeViewController != nil) {
        [activeViewController.view removeFromSuperview];
        activeViewController = nil;
    }
    
    activeViewController =  _categController;
    //  activeViewController.view.frame = CGRectMake(self.view.frame.size.width - PANEL_WIDTH, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self.view addSubview:activeViewController.view];
    [self addChildViewController:activeViewController];
    
    [activeViewController didMoveToParentViewController:self];
    
    [self showActiveViewWithShadow:YES withOffset:-2];
    
}
// Called by a view when its navButton is clicked and the panel is occupying the entire screen
- (void)movePanelRight {
    UIView *childView = [self getNavigationView];
    [self.view sendSubviewToBack:childView];
    
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         activeViewController.view.frame = CGRectMake(self.view.frame.size.width - PANEL_WIDTH, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             if (goToRoot)
                             {
                                 [self openRoot];
                                 goToRoot= NO;
                             }
                             

                         }
                     }];
}

// Called by a view when its navButton is clicked and the panel has already been moved to the right
- (void)movePanelToOriginalPosition {
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         activeViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             [self resetMainView];
                         }
                     }];
}

// Called by NavigationViewController when one of the coloured buttons is tapped
- (void)didSelectViewWithName:(NSString *)viewName {
    if (activeViewController != nil) {
        [activeViewController.view removeFromSuperview];
        activeViewController = nil;
    }
    [self showActiveViewWithName:viewName];
    [self movePanelToOriginalPosition];
}

#pragma mark -
#pragma mark UIGestureRecognizerDelegate methods

// This is where we can slide the active panel from left to right and back again,
// endlessly, for great fun!
-(void)movePanel:(id)sender {

    
    
    if ([activeViewController isKindOfClass:[MapController class]]) {
        return;
    }
    [[[(UITapGestureRecognizer*)sender view] layer] removeAllAnimations];
    
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
    CGPoint velocity = [(UIPanGestureRecognizer*)sender velocityInView:[sender view]];
    
    // Stop the main panel from being dragged to the left if it's not already dragged to the right
    if ((velocity.x < 0) && (activeViewController.view.frame.origin.x == 0)) {
        return;
    }
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        if(velocity.x > 0) {
            _showPanel = YES;
        }
        else {
            _showPanel = NO;
        }
        
        UIView *childView = [self getNavigationView];
        [self.view sendSubviewToBack:childView];
    }
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        // If we stopped dragging the panel somewhere between the left and right
        // edges of the screen, these will animate it to its final position.
        if (!_showPanel) {
            [self movePanelToOriginalPosition];
            _panelMovedRight = NO;
        } else {
            [self movePanelRight];
            _panelMovedRight = YES;
        }
    }
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged) {
        if(velocity.x > 0) {
            _showPanel = YES;
        }
        else {
            _showPanel = NO;
        }
        
        // Set the new x coord of the active panel...
        activeViewController.view.center = CGPointMake(activeViewController.view.center.x + translatedPoint.x, activeViewController.view.center.y);
        
        // ...and move it there
        [(UIPanGestureRecognizer*)sender setTranslation:CGPointMake(0, 0) inView:self.view];
    }
}

#pragma mark BaseDelegate

-(void)baseUpdated
{
    
    
    
    if (baseManager.currentType == BaseFile) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateFetch" object:nil userInfo:nil];
        
        [ServerCommunicaion downloadAndUnZip:CoordinatesFile downloadedFilePath:^(NSString *fileDownloadedName) {
            // dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            // dispatch_async(q, ^{
            
            [self UpdateLabel];
            [baseManager saveVersion:currentVersion];
            
            [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
            NSLog(@"%@",fileDownloadedName);
            [baseManager parseCSVCoordinates:fileDownloadedName];
            
            // });
            
        }];
    }
    if (baseManager.currentType == CoordinatesFile) 
    {
        NSLog(@"base finished");
        
        [downloadView removeFromSuperview];
    }
    
  
}

#pragma mark TopPanelDelegate

-(void)navButtonClicked:(int)navTag
{
    switch (navTag) {
        case 0: {
            [self movePanelToOriginalPosition];
        }
            break;
        case 1: {
            [self movePanelRight];
        }
            break;
            
        default:
            break;
    }

}

#pragma mark UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{

    if (buttonIndex == 1) {
        [self beginUpdate];
    }
}

@end
