//
//  CategController.m
//  SpravkaMagadan
//
//  Created by gelgard on 26.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "CategController.h"
#import "CategController_2.h"
#import "CategController_1.h"
#import "BaseManager.h"
static NSUInteger kNumberOfPages = 3;

@interface CategController ()

@end

@implementation CategController
{
    MapController *mapController;
    BaseManager *baseManager;
}

@synthesize pageController,scrollView,viewControllers, infoTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
   mainTitles = [[NSArray alloc] initWithObjects:@"ОСНОВНЫЕ КАТЕГОРИИ",@"ВСЕ КАТЕГОРИИ",@"ВСЕ ОРГАНИЗАЦИИ", nil];
   placeholders = [[NSArray alloc] initWithObjects:@"Поиск категории",@"Поиск категории",@"Поиск организации", nil];
    
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPages; i++) {
        
        NSString* className = [NSString stringWithFormat:@"CategController_%i",i];
        
        UIViewController *controller;
        
        NSString *nibName;
        
        nibName = [NSString stringWithFormat:@"CategScrollScreenView_%i_%@",i,[[MySingleton sharedMySingleton] getSystemVersion]  ]  ;
        
        
        controller = [[NSClassFromString(className) alloc] initWithNibName:nibName bundle:nil];
        
        [controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
  
	
    // a page is the width of the scroll view
    self.scrollView.pagingEnabled = YES;
    
    if (!IS_IPHONE5) {
        scrollHeight = 301;
    } else
        scrollHeight = 389;
    
    
//    CGRect framScroll = self.scrollView.frame;
//    framScroll.size.height = scrollHeight;
//    
//    [self.scrollView setFrame:framScroll];
    
    
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
	
    self.pageController.numberOfPages = kNumberOfPages;
    self.pageController.currentPage = 0;
	
    
    self.pageController.pageIndicatorTintColor = [UIColor blackColor];
    self.pageController.currentPageIndicatorTintColor = [UIColor whiteColor];
    // pages are created on demand
    // load the visible page
    // load the page on either side to avoid flashes when the user starts scrolling
    
  
    emptyView = [[EmptyInformationController alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openInformation:) name:@"openInformation" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(categorySelected:) name:@"categorySelected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(categorySelected:) name:@"closeInfo" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openMap:) name:@"openMap" object:nil];
    infoTableView = [[InfoTableController alloc] init];
    mapController = [[MapController alloc] init];
    
    self.searchField.clearButtonMode = UITextFieldViewModeAlways;
    // Do any additional setup after loading the view.
    
    baseManager = [[BaseManager alloc] init];
}


-(void)categorySelected: (NSNotification*)aNotification
{
  
    [emptyView dismissViewControllerAnimated:NO completion:^{
        
    }];
}

-(void)openMap: (NSNotification*)aNotification
{
    
   
    [mapController.view setFrame:self.view.frame];
    [informationView addSubview:mapController.view];
    [informationView bringSubviewToFront:mapController.view];
    if ([aNotification.object count]>0) {
        mapController.latitude = [[[aNotification.object objectAtIndex:0] objectForKey:@"latitude"] doubleValue];
        mapController.longitude = [[[aNotification.object objectAtIndex:0] objectForKey:@"longitude"] doubleValue];
         mapController.FromInfo = YES;
        if ([aNotification.object count]>1) {
            
           mapController.coordinatesArray = [NSArray arrayWithArray:aNotification.object];
            [mapController loadImageGroup];
            NSLog(@"NIAMA");
        } else
        {
            
            [mapController.selectTitle setText:[[aNotification.object  objectAtIndex:0] objectForKey:@"adress"]];
            
            
           
            [mapController loadEmbedMap];
        }
    }
    
    
   
}

#pragma mark MyAction

-(IBAction)searchStart:(id)sender
{
    NSString *tempVal;
    tempVal = ((UITextField *)sender).text;
    NSLog(@"CUR PAGE %i", (int)self.pageController.currentPage);
    
    int currentPage;
    currentPage = (int)self.pageController.currentPage;
    
    if (currentPage == 0) {
        [self.scrollView scrollRectToVisible:CGRectMake(self.view.bounds.size.width, 0, self.view.bounds.size.width, self.view.bounds.size.height) animated:NO];
        [((CategController_1 *)[viewControllers objectAtIndex:1]) updateFetchWithFilter:tempVal];
        [self.searchField setText:tempVal];
    }
     if (currentPage == 1) {
         [((CategController_1 *)[viewControllers objectAtIndex:1]) updateFetchWithFilter:self.searchField.text];
     }
    if (currentPage == 2) {
        [((CategController_2 *)[viewControllers objectAtIndex:2]) updateFetchWithFilter:self.searchField.text categName:@""];
    }
    
    if([((UITextField *)sender).text isEqualToString:@""] || ((UITextField *)sender).text == nil)
        [(UITextField *)sender resignFirstResponder];

    
}


-(IBAction)cancelClick:(id)sender
{

    if([((UITextField *)sender).text isEqualToString:@""] || ((UITextField *)sender).text == nil)
        [(UITextField *)sender resignFirstResponder];

    
}


-(void)showInfo:(NSIndexPath *)currentRow
{
    NSManagedObject *org = [fetchResult objectAtIndexPath:currentRow];
    currentIndexPath = currentRow;
    
    [infoTableView.nameAdress removeAllObjects];
    
    NSMutableArray *adressArray = [[NSMutableArray alloc] init];
    [adressArray removeAllObjects];
    NSString *adress = [NSString stringWithFormat:@"%@",[org valueForKey:@"adress"]];
    
    [infoTableView.nameAdress addObject:[org valueForKey:@"adress"]];
    
    if ([[org valueForKey:@"info"] length]>0) {
        [infoTableView.nameAdress addObject:[org valueForKey:@"info"]];
    }
    
   NSLog(@"NAME %@",[org valueForKey:@"name"]);
    
    
    
    NSMutableArray *categArrays = [[NSMutableArray alloc] init];
    
    for (NSDictionary* orgs in [baseManager getCategoriesForNames:[org valueForKey:@"name"]] ) {
                [categArrays addObject:[orgs objectForKey:@"categ"]];
    }
    
    [infoTableView.nameAdress addObject:categArrays];
    
    [infoTableView.phones removeAllObjects];
    

    if ([self getCoordinatesFromComent:adress] != nil) {
        [adressArray addObject:[NSDictionary dictionaryWithDictionary:[self getCoordinatesFromComent:adress]]];
    }
    
    for (int i=1; i<=10; i++) {

        if ([[org valueForKey:[NSString stringWithFormat:@"phone_%i",i]] length]>0) {
            
            [infoTableView.phones addObject:[[NSDictionary alloc] initWithObjectsAndKeys:[org valueForKey:[NSString stringWithFormat:@"phone_%i",i]],@"phone",[org valueForKey:[NSString stringWithFormat:@"coment_%i",i]],@"comment", nil]];
            
            if ([self getCoordinatesFromComent:[org valueForKey:[NSString stringWithFormat:@"coment_%i",i]]] != nil) {
                [adressArray addObject:[NSDictionary dictionaryWithDictionary:[self getCoordinatesFromComent:[org valueForKey:[NSString stringWithFormat:@"coment_%i",i]]]]];
            }
            
        }
    }
    
    

    [infoName setText:[org valueForKey:@"name"]];
    
    
    if ([adressArray count]>0) {
        [infoTableView.nameAdress addObject:adressArray];
    }
   
    
    
    
    NSLog(@"DSC %@",[infoTableView.nameAdress description]);
    [infoTableView.selArray removeAllObjects];
    [infoTableView.infoTable reloadData];
}


-(NSDictionary *)getCoordinatesFromComent:(NSString *)adress
{
    NSDictionary *res;
    
    NSRange range = [adress rangeOfString:@", "];
    
    
    if (range.location != NSNotFound) {
        NSString *street = [adress componentsSeparatedByString:@", "][0];
        NSString *home = [adress componentsSeparatedByString:@", "][1];
        
        NSDictionary *coordinates = [NSDictionary dictionaryWithDictionary:[baseManager getAdressForStreet:street home:home]];
        
        
        
        if ([[coordinates allKeys] count]>0) {
            //   NSLog(@"s %@ h %@ lat %@ long %@", street, home, [[baseManager getAdressForStreet:street home:home] objectForKey:@"latitude"],[[baseManager getAdressForStreet:street home:home] objectForKey:@"longitude"]);
            //[infoTableView.nameAdress addObject:coordinates];
            
            return coordinates;
            
        }
    }
    
    return nil;
}

-(void)openInformation: (NSNotification*)aNotification
{
    currentIndex = (int)((NSIndexPath *)[aNotification.object objectForKey:@"indexPath"]).row;
    
    fetchResult = ((NSFetchedResultsController *)[aNotification.object objectForKey:@"fetch"]);
    [self showInfo:((NSIndexPath *)[aNotification.object objectForKey:@"indexPath"])];
    
    
    
    
         [emptyView.view setAlpha:1];
    [self presentViewController:emptyView animated:NO completion:^{
        [UIView animateWithDuration:.5 animations:^{
       
        } completion:^(BOOL success) {
            if (success) {
                
                
            }
        }];
    }];  //  [self.view bringSubviewToFront:informationView];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    self.scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollHeight);
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    
    [self loadScrollViewWithPage:2];
    
    [emptyView.view setFrame:self.view.bounds];
    [emptyView.view setAlpha:0];
    [informationView setFrame:self.view.bounds];
    [emptyView.view addSubview:informationView];
    [self.view addSubview:emptyView.view];
    infoTableView.selfFrame = self.infoTable.bounds;
   [infoTableView.view setFrame:self.infoTable.bounds];
    
    [self.infoTable addSubview:infoTableView.view];
    [super viewDidAppear:YES];
    
        }
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ScrollView


- (void)loadScrollViewWithPage:(int)page {
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
	
    // replace the placeholder if necessary
    UIViewController *controller = [viewControllers objectAtIndex:page];
    if ((NSNull *)controller == [NSNull null]) {
        
        NSString* className = [NSString stringWithFormat:@"CategController_%i",page];
        
        
        
        NSString *nibName;
        
        nibName = [NSString stringWithFormat:@"CategScrollScreenView_%i_%@",page,[[MySingleton sharedMySingleton] getSystemVersion]  ]  ;
        
        
        controller = [[NSClassFromString(className) alloc] initWithNibName:nibName bundle:nil];

       
       
        [viewControllers replaceObjectAtIndex:page withObject:controller];
       
    }
	
   
    // add the controller's view to the scroll view
    
    
    if (nil == controller.view.superview) {
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        
       // frame.size.height = scrollHeight;
        controller.view.frame = frame;
        [self.scrollView addSubview:controller.view];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (pageControlUsed) {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageController.currentPage = page;
	
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    NSDictionary *titleInfo = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[mainTitles objectAtIndex:(int)self.pageController.currentPage]],@"title", [NSString stringWithFormat:@"%@",[placeholders objectAtIndex:(int)self.pageController.currentPage]],@"placeholder",nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeTitle" object:titleInfo userInfo:nil];
	
    
    [self.mainTitle setText:[titleInfo objectForKey:@"title"]];
    
  
    [self.searchField setText:@""];
    self.searchField.placeholder = [titleInfo objectForKey:@"placeholder"];
    // A possible optimization would be to unload the views+controllers which are no longer visible
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlUsed = NO;
    
    
    
    //NSLog(@"CUR PAGE %i",(int)self.pageController.currentPage);
    
    switch ((int)self.pageController.currentPage) {
        case 1:
            [((CategController_1 *)[viewControllers objectAtIndex:(int)self.pageController.currentPage]) refreshData];
            break;
            
        case 2:
            [((CategController_2 *)[viewControllers objectAtIndex:(int)self.pageController.currentPage]) refreshData];
            break;
        default:
            break;
    }
}

- (IBAction)changePage:(id)sender {
    int page = (int)self.pageController.currentPage;
    
    
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    // update the scroll view to the appropriate page
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    // Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}


-(IBAction)navBtnClk:(id)sender
{
    
    [_topDelegate navButtonClicked:(int)self.navButton.tag];
    
    [self.searchField resignFirstResponder];
    
    if (self.navButton.tag==1) {
        self.navButton.tag = 0;
    } else
        self.navButton.tag = 1;
    
    
}


-(IBAction)closeInfoView:(id)sender
{
//    [UIView animateWithDuration:.5 animations:^{
//        [informationView setAlpha:0];
//    } completion:^(BOOL success) {
//        if (success) {
//            
//        }
//    }];
    
    [emptyView dismissViewControllerAnimated:NO completion:^{
        
    }];
}


-(IBAction)navigateByOrganiz:(id)sender
{
    int direction;
    direction = (int)((UIButton *)sender).tag;
    
    
    if (direction == 0) direction = -1;

      
    if (currentIndexPath.row == 0 && direction == -1) return;
    if (currentIndexPath.row == ([[[fetchResult sections] objectAtIndex:0] numberOfObjects]-1) && direction == 1) return;
    
   NSIndexPath *newIndex=[NSIndexPath indexPathForRow:(currentIndexPath.row + direction) inSection:0];
        [self showInfo:newIndex];
        currentIndexPath = newIndex;
    
    
   
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
