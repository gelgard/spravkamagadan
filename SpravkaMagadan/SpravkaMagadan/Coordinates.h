//
//  Coordinates.h
//  SpravkaMagadan
//
//  Created by Oleg on 01.03.15.
//  Copyright (c) 2015 uran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Coordinates : NSManagedObject

@property (nonatomic, retain) NSString * home;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * street;
@property (nonatomic, retain) NSNumber * ord;

@end
