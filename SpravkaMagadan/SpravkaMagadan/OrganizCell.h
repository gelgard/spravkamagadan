//
//  OrganizCell.h
//  SpravkaMagadan
//
//  Created by gelgard on 27.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrganizCell : UITableViewCell


@property (nonatomic,strong) IBOutlet UILabel *categName;
@property (nonatomic,strong) IBOutlet UILabel *adresName;

-(void)animatedShow;

@end
