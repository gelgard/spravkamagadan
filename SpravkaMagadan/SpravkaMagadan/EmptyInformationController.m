//
//  EmptyInformationController.m
//  SpravkaMagadan
//
//  Created by gelgard on 07.05.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "EmptyInformationController.h"

@interface EmptyInformationController ()

@end

@implementation EmptyInformationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
