@protocol BaseDelegate <NSObject>

@required
// A child panel calls these methods in its delegate when the nav button is clicked
// Which one is called depends on the tag of the navButton, which toggles each time
// it's tapped.
- (void)baseUpdated;
@end
