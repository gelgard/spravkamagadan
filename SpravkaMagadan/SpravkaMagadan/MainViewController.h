#import "BaseManager.h"
#import "FXBlurView.h"

@interface MainViewController : UIViewController <UIAlertViewDelegate, BaseDelegate>
{
    BOOL goToRoot;
    BaseManager *baseManager;
    
    IBOutlet UIView *downloadViewLabel;
    IBOutlet UILabel *downloadLabel;

    int currentVersion;
}

-(void)beginUpdate;

@property (nonatomic, strong) IBOutlet FXBlurView *downloadView;

@end
