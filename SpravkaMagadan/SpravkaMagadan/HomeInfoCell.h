//
//  HomeInfoCell.h
//  SpravkaMagadan
//
//  Created by gelgard on 11.05.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HomeInfoCell : UITableViewCell
{
   
}


@property (nonatomic, strong) IBOutlet UIImageView *picImage;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *moreLabel;


@property (nonatomic, strong) IBOutlet UIView *adresView;
@property (nonatomic, strong) IBOutlet UIView *phoneView;


@property (nonatomic, strong) IBOutlet UILabel *phoneLabel;
@property (nonatomic, strong) IBOutlet UILabel *adresPhoneLabel;
@property (nonatomic, strong) IBOutlet UITextView *categViews;




-(void)generateCategView:(NSArray *)categArray;

@end
