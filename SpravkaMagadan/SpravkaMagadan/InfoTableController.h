//
//  InfoTableController.h
//  SpravkaMagadan
//
//  Created by gelgard on 11.05.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeInfoCell.h"
#import "MapController.h"

@interface InfoTableController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    MapController *mapView;
}

@property (nonatomic, strong)  IBOutlet UITableView *infoTable;
@property (nonatomic, strong)  NSMutableArray* nameAdress;
@property (nonatomic, strong)  NSMutableArray* phones;
@property (nonatomic, strong) NSMutableArray* selArray;
@property CGRect selfFrame;;
@end
