//
//  ServerCommunicaion.m
//  SpravkaMagadan
//
//  Created by gelgard on 04.05.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "ServerCommunicaion.h"
#import "AFNetworking.h"
#import "ZipArchive.h"


@implementation ServerCommunicaion
{
    
}




+(void)downloadAndUnZip:(ServerFileType)fileType downloadedFilePath:(void (^)(NSString* fileDownloadedName))fileDownloaded
{
   
    

    
    NSString *fileUrl;
     NSString *fileName;
    switch (fileType) {
        case BaseFile:
            fileUrl = serverBasePath;
            fileName = @"base.zip";
            break;
        case VersionFile:
            fileUrl = serverVresionPath;
            fileName = @"version.txt";
            break;
            
            case CoordinatesFile:
            fileUrl = serverCoordinatesPath;
            fileName = @"adres.zip";
            break;
            
        default:
            break;
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:fileUrl]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request] ;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
    NSLog(@"%@ %@ %@",fileUrl, fileName, path);
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
    
        if (fileType == BaseFile) {
            ZipArchive *za = [[ZipArchive alloc] init];
            if ([za UnzipOpenFile: path]) {
                // 2
                BOOL ret = [za UnzipFileTo:[paths objectAtIndex:0] overWrite: YES];
                if (NO == ret){} [za UnzipCloseFile];
                fileDownloaded([[paths objectAtIndex:0] stringByAppendingPathComponent:csvFile]);
            }
        }
        if (fileType == VersionFile) {
            fileDownloaded([[paths objectAtIndex:0] stringByAppendingPathComponent:versionFileName]);
        }
        
        if (fileType == CoordinatesFile) {
            ZipArchive *za = [[ZipArchive alloc] init];
            if ([za UnzipOpenFile: path]) {
                // 2
                BOOL ret = [za UnzipFileTo:[paths objectAtIndex:0] overWrite: YES];
                if (NO == ret){} [za UnzipCloseFile];
                fileDownloaded([[paths objectAtIndex:0] stringByAppendingPathComponent:coordinatesFileName]);
            }
        }
        
       
       
        
            // 4
        
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
              fileDownloaded (@"");
    }];
    
    [operation start];
    
  
}

@end
