//
//  BaseManager.m
//  SpravkaMagadan
//
//  Created by gelgard on 05.05.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "BaseManager.h"
#import "Coordinates.h"


@implementation BaseManager
{
    int homeCount;
}

@synthesize managedObjectContext, delegateBase;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize currentType;


- (id)init
{
    self = [super init];
    if (self)
    {
        // Initialization code here.
        
        id delegate = [[UIApplication sharedApplication] delegate];
        self.managedObjectContext = [delegate managedObjectContext];
        fieldKeysCoord = [[NSArray alloc] initWithObjects:@"street",@"home",@"latitude",@"longitude", nil];
        fieldKeys = [[NSArray alloc] initWithObjects:@"categ",@"name",@"adress",@"info",@"phone_1",@"coment_1",@"phone_2",@"coment_2",@"phone_3",@"coment_3",@"phone_4",@"coment_4",@"phone_5",@"coment_5",@"phone_6",@"coment_6",@"phone_7",@"coment_7",@"phone_8",@"coment_8",@"phone_9",@"coment_9",@"phone_10",@"coment_10",@"iconlink",@"bold",@"order",@"maincategorder",nil];
    }
    
    return self;
}

#pragma mark DatabaseAction

-(BOOL) isDBEmpty
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Organization" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
  
    
    NSArray *res = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    return [res count] > 0;
}

-(NSMutableArray *)getCategories
{
    NSMutableArray *resArray;
    
    
    
    
    return resArray;
}


- (NSString *) saveFilePath
{
	NSArray *path =
	NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
	return [[path objectAtIndex:0] stringByAppendingPathComponent:@"version.plist"];
    
}

- (void) saveVersion:(int)version
{
	NSArray *values = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:version],nil];
	[values writeToFile:[self saveFilePath] atomically:YES];

    
}

-(BOOL)checkDatabaseVersion:(int)currentVersion
{
    BOOL res=NO;
    
    
    NSString *myPath = [self saveFilePath];
    
	BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:myPath];
    
	if (fileExists)
	{
        
		NSArray *values = [[NSArray alloc] initWithContentsOfFile:myPath];
		res = ([[values objectAtIndex:0] integerValue] < currentVersion);
        
        if (res) {
          //  [self saveVersion:currentVersion];
        }
	}else
    {
        [self saveVersion:0];
        res = YES;
    }
    return res;
}


-(int)getDatabaseVersion
{
    NSString *myPath = [self saveFilePath];
    
	BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:myPath];
    if (fileExists)
	{NSArray *values = [[NSArray alloc] initWithContentsOfFile:myPath];
        return [[values objectAtIndex:0] integerValue];
    } else
        return 0;
    
}
-(void)insertLinesCoordinatesIndb:(NSMutableArray *)lineArray
{
    
    
    

    
    NSError *error;
    
    Organization *coreTable= [NSEntityDescription
                              insertNewObjectForEntityForName:@"Coordinates"
                              inManagedObjectContext:self.managedObjectContext];
    NSLog(@"LARR %lu",(unsigned long)[lineArray count]);
    for (int i=0; i<[lineArray count]; i++) {
        
        
        NSLog(@"LARR %lu",(unsigned long)i);
        if (i==2 || i==3) {
            NSNumber *inVal = [NSNumber numberWithFloat:[[lineArray objectAtIndex:i] floatValue]];
            
            [coreTable setValue:inVal forKey:[fieldKeysCoord objectAtIndex:i]];
        } else
        {
            NSString *inVal = [NSString stringWithFormat:@"%@",[lineArray objectAtIndex:i]];
            if (i == 1 && ((NSString *)[inVal stringByReplacingPercentEscapesUsingEncoding:NSWindowsCP1251StringEncoding]).length == 0) {
                [coreTable setValue:[NSString stringWithFormat:@"---"] forKey:[fieldKeysCoord objectAtIndex:i]];
            } else
            {
                
                
                [coreTable setValue:[inVal stringByReplacingPercentEscapesUsingEncoding:NSWindowsCP1251StringEncoding] forKey:[fieldKeysCoord objectAtIndex:i]];
            }
          
        }
       
        
        
        
        
        
    }
    [coreTable setValue:[NSNumber numberWithInteger:homeCount] forKey:@"ord"];
    
    if(![self.managedObjectContext save:&error]){
        NSLog(@"Unresolved error: %@, %@", error, [error userInfo]);
        abort();
    }
    
}
-(void)insertLinesIndb:(NSMutableArray *)lineArray
{
    
    
 
 
    
     NSError *error;
    
    Organization *coreTable= [NSEntityDescription
                              insertNewObjectForEntityForName:@"Organization"
                              inManagedObjectContext:self.managedObjectContext];
    NSLog(@"DESC %lu %@",(unsigned long)[lineArray count],[lineArray description]);
    
    if ([lineArray count] < 27)
    {
        return;
    }
    for (int i=0; i<28; i++) {
     
        
        if (i==26 || i == 27) {
            int intVal =(int)[((NSString *)[lineArray objectAtIndex:i]) integerValue];
            if (!(intVal >0) )
               [coreTable setValue:[NSNumber numberWithInt:10000] forKey:[fieldKeys objectAtIndex:i]];
            else
            [coreTable setValue:[NSNumber numberWithInt:intVal] forKey:[fieldKeys objectAtIndex:i]];
        }
        else
        {
            
            NSString *inVal = [NSString stringWithFormat:@"%@",[lineArray objectAtIndex:i]];
            
            [coreTable setValue:[inVal stringByReplacingPercentEscapesUsingEncoding:NSWindowsCP1251StringEncoding] forKey:[fieldKeys objectAtIndex:i]];
        }
        
        
        
    }
    
   
    if(![self.managedObjectContext save:&error]){
        NSLog(@"Unresolved error: %@, %@", error, [error localizedDescription]);
        abort();
    }
        
}

-(void)parseCSVCoordinates:(NSString *)fileName
{
    
    NSLog(@"fn %@",fileName);
    NSError *error;
    NSFetchRequest * fetch1 = [[NSFetchRequest alloc] init] ;
    [fetch1 setEntity:[NSEntityDescription entityForName:@"Coordinates" inManagedObjectContext:self.managedObjectContext]];
    
    
    
    
    
    NSArray * result1 = [self.managedObjectContext executeFetchRequest:fetch1 error:nil];
    
    
    
    for (id tovar in result1)
    {
        
        
        [self.managedObjectContext deleteObject:tovar];
        
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
    NSLog(@"Beginning...");
    self.currentType = CoordinatesFile;
    homeCount = 0;
    
    
    
    
    NSStringEncoding encoding = 11;
    NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:fileName];
    CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:';'];
    [p setRecognizesBackslashesAsEscapes:YES];
    [p setSanitizesFields:YES];
    
    
    
    p.delegate = self;
    [p parse];
//    if ([[UIDevice currentDevice] isMultitaskingSupported]) {
//        UIApplication *application = [UIApplication sharedApplication];
//        __block UIBackgroundTaskIdentifier background_task;
//        background_task = [application beginBackgroundTaskWithExpirationHandler: ^ {
//            [application endBackgroundTask: background_task];
//            background_task = UIBackgroundTaskInvalid;
//        }];
//        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            
//            //DO STUFF IN BACKGROUND HERE
//           
//            [application endBackgroundTask: background_task];
//            background_task = UIBackgroundTaskInvalid;
//        });
//    }

}


-(void)parseCSV:(NSString *)fileName
{
    
    
    NSError *error;
    NSFetchRequest * fetch1 = [[NSFetchRequest alloc] init] ;
    [fetch1 setEntity:[NSEntityDescription entityForName:@"Organization" inManagedObjectContext:self.managedObjectContext]];
    
    
    
    
    
    NSArray * result1 = [self.managedObjectContext executeFetchRequest:fetch1 error:nil];
    
    
    
    for (id tovar in result1)
    {
        
        
        [self.managedObjectContext deleteObject:tovar];
        
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
    NSLog(@"Beginning...");
	
    NSStringEncoding encoding = 11;
    NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:fileName];
    NSLog(@"%@",fileName);
    CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:';'];
    [p setRecognizesBackslashesAsEscapes:YES];
    [p setSanitizesFields:YES];
    
    
    
    p.delegate = self;
    [p parse];
	
//    if ([[UIDevice currentDevice] isMultitaskingSupported]) {
//        UIApplication *application = [UIApplication sharedApplication];
//        __block UIBackgroundTaskIdentifier background_task;
//        background_task = [application beginBackgroundTaskWithExpirationHandler: ^ {
//            [application endBackgroundTask: background_task];
//            background_task = UIBackgroundTaskInvalid;
//        }];
//        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            
//            //DO STUFF IN BACKGROUND HERE
//            
//            NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
//           // NSLog(@"raw difference: %f", (end-start));
//            [application endBackgroundTask: background_task];
//            background_task = UIBackgroundTaskInvalid;
//        });
//    }
	

    
    // Perform task here
    
	
    
 //   NSLog(@"%@", [p.delegate lines]);

}

#pragma mark Parser Delegate

- (void)parserDidBeginDocument:(CHCSVParser *)parser {
   
    _lines = [[NSMutableArray alloc] init];
    [_lines removeAllObjects];
}
- (void)parser:(CHCSVParser *)parser didBeginLine:(NSUInteger)recordNumber {
    _currentLine = [[NSMutableArray alloc] init];
}
- (void)parser:(CHCSVParser *)parser didReadField:(NSString *)field atIndex:(NSInteger)fieldIndex {
  // NSLog(@"%@", [field dataUsingEncoding:NSWindowsCP1251StringEncoding]);
  
    [_currentLine addObject:field];
}
- (void)parser:(CHCSVParser *)parser didEndLine:(NSUInteger)recordNumber {
    
    
  //  NSLog(@"Record number %lu",(unsigned long)recordNumber);
    if (self.currentType == BaseFile) {
      //  NSLog(@"CR %@",[_currentLine description]);
        [self insertLinesIndb:_currentLine];
    } else
    {
        homeCount++;
        [self insertLinesCoordinatesIndb:_currentLine];

    }
    

   //NSLog(@"LINE %lu",(unsigned long)[_currentLine count]);
     // sleep(2);
    _currentLine = nil;
}
- (void)parserDidEndDocument:(CHCSVParser *)parser {
    //	NSLog(@"parser ended: %@", [[_lines objectAtIndex:0] description]);
    
    if (self.currentType == BaseFile || self.currentType == CoordinatesFile) {
        [self.delegateBase baseUpdated];
    }
    
    homeCount = 0;
}
- (void)parser:(CHCSVParser *)parser didFailWithError:(NSError *)error {
	NSLog(@"ERROR: %@", error);
    _lines = nil;
}



#pragma mark fetch request

-(NSDictionary *)getAdressForStreet:(NSString*)street home:(NSString *)home
{
    NSArray *resArray;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Coordinates" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"street = '%@' AND home = '%@'", street, home]]];
    [fetchRequest setEntity:entity];
    
    
    NSError *error;
    
    
    resArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    
    if ([resArray count]==0)
    {
        return nil;
    }
    
    
    return [NSDictionary dictionaryWithObjects:@[((Coordinates *)[resArray objectAtIndex:0]).latitude,((Coordinates *)[resArray objectAtIndex:0]).longitude , [NSNumber numberWithInt:5],[NSString stringWithFormat:@"%@, %@",street,home]] forKeys:@[@"latitude",@"longitude",@"btntag",@"adress"]];
    
}
- (NSFetchedResultsController *)fetchedResultsControllerAdress:(NSString* )street {
    
    NSString *key;
    NSString *keySort;
    if (street.length == 0) {
        key = [NSString stringWithFormat:@"%@",@"street"];
        keySort = [NSString stringWithFormat:@"%@",@"street"];
    } else
    {
        key = [NSString stringWithFormat:@"%@",@"home"];
        keySort = [NSString stringWithFormat:@"%@",@"ord"];
    }
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Coordinates" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:keySort ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    
    
    
    [fetchRequest setResultType:NSDictionaryResultType ];
    
    if (street.length == 0)
    {
        [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:key]];
        [fetchRequest setPropertiesToGroupBy:[NSArray arrayWithObject:key]];
    } else
    {
        [fetchRequest setPropertiesToFetch:@[key,@"latitude",@"longitude",@"street"]];
       
    }
    
    
    if ([street length]>0) {
                    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"street = '%@'", street]]];
        
    } else
    {
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"street != ''"]]];
    }
    
    [fetchRequest setFetchBatchSize:20];
    
    
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil
                                                   cacheName:nil];
    // self.fetchedResultsController = theFetchedResultsController;
    theFetchedResultsController.delegate = self;
    NSError *error = nil;
    if(![theFetchedResultsController performFetch:&error]){
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    

    return theFetchedResultsController;
    
}

- (NSFetchedResultsController *)fetchedResultsControllerCategory:(NSString* )categoryName full:(BOOL)full  {
    

    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Organization" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];

    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"categ" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    
    

    [fetchRequest setResultType:NSDictionaryResultType ];
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:@"categ"]];
    [fetchRequest setPropertiesToGroupBy:[NSArray arrayWithObject:@"categ"]];

    if ([categoryName length]>0) {
        NSLog(@"CATEG %@",categoryName);
        if (full) {
            [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"categ = '%@'", categoryName]]];
        } else
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"categ CONTAINS[cd] '%@'", categoryName]]];
    } else
    {
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"categ != ''"]]];
    }
    
    [fetchRequest setFetchBatchSize:20];
    
   
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil
                                                   cacheName:nil];
   // self.fetchedResultsController = theFetchedResultsController;
    theFetchedResultsController.delegate = self;
    NSError *error = nil;
    if(![theFetchedResultsController performFetch:&error]){
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return theFetchedResultsController;
    
}


-(NSArray *)getCategoriesForNames:(NSString*)name
{
    NSArray *resArray;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Organization" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ AND categ != ''", name]];
[fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"categ", nil]];
    [fetchRequest setPropertiesToGroupBy:[NSArray arrayWithObjects:@"categ",nil]];
[fetchRequest setResultType:NSDictionaryResultType];
    [fetchRequest setEntity:entity];
    NSError *error;


    resArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];






    return [resArray mutableCopy];

}

- (NSFetchedResultsController *)fetchedResultsControllerOrganization:(NSString* )categoryName name:(NSString *)name{
    
  
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Organization" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor *sort, *sort1;
    
    
    if ([categoryName length]>0) {
        sort = [[NSSortDescriptor alloc]
                initWithKey:@"name" ascending:YES];
      
        sort1 = [[NSSortDescriptor alloc]
                initWithKey:@"order" ascending:YES];
        [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sort1, sort,nil]];
        
        [fetchRequest setResultType:NSDictionaryResultType ];
        [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"name",@"adress",@"phone_1",@"phone_2",@"phone_3",@"phone_4",@"phone_5",@"phone_6",@"phone_7",@"phone_8",@"phone_9",@"phone_10", @"coment_1",@"coment_2",@"coment_3",@"coment_4",@"coment_5",@"coment_6",@"coment_7",@"coment_8",@"coment_9",@"coment_10",@"info",@"order",nil]];
        [fetchRequest setPropertiesToGroupBy:[NSArray arrayWithObjects:@"name",@"adress",@"phone_1",@"phone_2",@"phone_3",@"phone_4",@"phone_5",@"phone_6",@"phone_7",@"phone_8",@"phone_9",@"phone_10", @"coment_1",@"coment_2",@"coment_3",@"coment_4",@"coment_5",@"coment_6",@"coment_7",@"coment_8",@"coment_9",@"coment_10", @"info",@"order",nil]];
        
    } else
    {
        sort = [[NSSortDescriptor alloc]
                initWithKey:@"name" ascending:YES];
        sort1 = [[NSSortDescriptor alloc]
                 initWithKey:@"order" ascending:YES];
        
        [fetchRequest setResultType:NSDictionaryResultType ];
        [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"name",@"adress",@"phone_1",@"phone_2",@"phone_3",@"phone_4",@"phone_5",@"phone_6",@"phone_7",@"phone_8",@"phone_9",@"phone_10", @"coment_1",@"coment_2",@"coment_3",@"coment_4",@"coment_5",@"coment_6",@"coment_7",@"coment_8",@"coment_9",@"coment_10",@"info",nil]];
        [fetchRequest setPropertiesToGroupBy:[NSArray arrayWithObjects:@"name",@"adress",@"phone_1",@"phone_2",@"phone_3",@"phone_4",@"phone_5",@"phone_6",@"phone_7",@"phone_8",@"phone_9",@"phone_10", @"coment_1",@"coment_2",@"coment_3",@"coment_4",@"coment_5",@"coment_6",@"coment_7",@"coment_8",@"coment_9",@"coment_10", @"info",nil]];
        [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sort, nil]];
    }
    
    NSLog(@" categ %@ name %@",categoryName, name);
    if ([categoryName length]>0 && [name length]>0 ) {
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"categ = %@ AND name CONTAINS[cd] %@", categoryName,name]];
    }
    
    if ([categoryName length]>0 && [name length]==0 ) {
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"categ = %@", categoryName]];
    }
    
    if ([categoryName length]==0) {
        
         if ([name length]>0)
         {
             [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ AND categ != ''", name]];
         } else
             [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"categ != ''", name]];
        
    }
    
    
   
    
//    sort = [[NSSortDescriptor alloc]
//            initWithKey:@"order" ascending:YES];
//    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
//    
    
    
    
    [fetchRequest setFetchBatchSize:20];
    
    //NSArray *res = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil
                                                   cacheName:nil];
   
    
    theFetchedResultsController.delegate = self;
    NSError *error = nil;
    if(![theFetchedResultsController performFetch:&error]){
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return theFetchedResultsController;
    
}
@end
