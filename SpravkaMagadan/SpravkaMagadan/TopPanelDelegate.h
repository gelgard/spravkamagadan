//
//  TopPanelDelegate.h
//  SpravkaMagadan
//
//  Created by gelgard on 26.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

@protocol TopPanelDelegate <NSObject>

@required
// A child panel calls these methods in its delegate when the nav button is clicked
// Which one is called depends on the tag of the navButton, which toggles each time
// it's tapped.
-(void)navButtonClicked:(int)navTag;

@end

