//
//  HomeInfoCell.m
//  SpravkaMagadan
//
//  Created by gelgard on 11.05.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "HomeInfoCell.h"

@implementation HomeInfoCell

@synthesize picImage = _picImage;
@synthesize nameLabel = _nameLabel;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



-(void)generateCategView:(NSArray *)categArray
{
    NSLog(@"-- %@", [categArray description]);
    [_categViews setAlpha:1];
    
    NSMutableAttributedString *paragraph = [[NSMutableAttributedString alloc] init];
    NSString *divider;
    divider = [NSString stringWithFormat:@"%@", @""];
    for (NSString* str in categArray) {
        NSMutableAttributedString *attStringDiv = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",divider] attributes:nil];
        
        divider = [NSString stringWithFormat:@"%@", @" ,"];
         [paragraph appendAttributedString:attStringDiv];
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",str] attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),NSBackgroundColorAttributeName: [UIColor clearColor],NSForegroundColorAttributeName: [UIColor colorWithRed:1 green:204.0f/255.0f blue:51.0f/255.0f alpha:1],@"categText": str, NSFontAttributeName: [UIFont systemFontOfSize:18]}];
        
        divider = [NSString stringWithFormat:@"%@", @" ,"];
        [paragraph appendAttributedString:attString];
       
    }
    
    _categViews.attributedText = paragraph;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textTapped:)];
    tap.delegate = self;
    [_categViews addGestureRecognizer:tap];
}

#pragma mark Attributed
- (NSAttributedString *)attributedTextViewString
{
    NSMutableAttributedString *paragraph = [[NSMutableAttributedString alloc] initWithString:@"This is a string with " attributes:@{NSForegroundColorAttributeName:[UIColor blueColor]}];
    
    NSAttributedString* attributedString = [[NSAttributedString alloc] initWithString:@"a tappable string"
                                                                           attributes:@{@"tappable":@(YES),
                                                                                        @"networkCallRequired": @(YES),
                                                                                        @"loadCatPicture": @(NO)}];
    
    NSAttributedString* anotherAttributedString = [[NSAttributedString alloc] initWithString:@" and another tappable string"
                                                                                  attributes:@{@"tappable":@(YES),
                                                                                               @"networkCallRequired": @(NO),
                                                                                               @"loadCatPicture": @(YES)}];
    [paragraph appendAttributedString:attributedString];
    [paragraph appendAttributedString:anotherAttributedString];
    
    return [paragraph copy];
}

- (void)textTapped:(UITapGestureRecognizer *)recognizer
{
    UITextView *textView = (UITextView *)recognizer.view;
    
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    NSLog(@"location: %@", NSStringFromCGPoint(location));
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        NSDictionary *attributes = [textView.textStorage attributesAtIndex:characterIndex effectiveRange:&range];
        
        NSLog(@"attributes %@", [attributes objectForKey:@"categText"]);
        NSDictionary *titleInfo = [[NSDictionary alloc] initWithObjectsAndKeys:[attributes objectForKey:@"categText"],@"title", [NSString stringWithFormat:@"%@",@"111"],@"placeholder",nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"categorySelected" object:titleInfo userInfo:nil];
    
        
        //Based on the attributes, do something
        ///if ([attributes objectForKey:...)] //make a network call, load a cat Pic, etc
        
    }
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([[gestureRecognizer view] isKindOfClass:[UITableViewCell class]])
        return NO;
        return YES;
        }

@end
