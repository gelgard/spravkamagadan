//
//  Organization.m
//  SpravkaMagadan
//
//  Created by gelgard on 06.05.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "Organization.h"


@implementation Organization

@dynamic categ;
@dynamic name;
@dynamic adress;
@dynamic info;
@dynamic phone_1;
@dynamic coment_1;
@dynamic phone_2;
@dynamic phone_3;
@dynamic phone_4;
@dynamic phone_5;
@dynamic phone_6;
@dynamic phone_7;
@dynamic phone_8;
@dynamic phone_9;
@dynamic phone_10;
@dynamic coment_2;
@dynamic coment_3;
@dynamic coment_4;
@dynamic coment_5;
@dynamic coment_6;
@dynamic coment_7;
@dynamic coment_8;
@dynamic coment_9;
@dynamic coment_10;
@dynamic iconlink;
@dynamic bold;
@dynamic order;
@dynamic maincategorder;

@end
