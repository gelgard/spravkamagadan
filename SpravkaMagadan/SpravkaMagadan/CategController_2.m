//
//  CategController_2.m
//  SpravkaMagadan
//
//  Created by gelgard on 26.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "CategController_2.h"
#import "Organization.h"

@interface CategController_2 ()

@end

@implementation CategController_2
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize categFilter;
@synthesize isSelected;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil filter:(NSString *)categName
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.categFilter = categName;
        self.isSelected = YES;
        // Custom initialization
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
     self.categFilter = @"";
        self.isSelected = NO;
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    baseManager = [[BaseManager alloc] init];
    NSError *error;
    if (![[baseManager fetchedResultsControllerOrganization:self.categFilter name:@""] performFetch:&error]) {
        // Update to handle the error appropriately.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        exit(-1);  // Fail
        
    }
    _fetchedResultsController = [baseManager fetchedResultsControllerOrganization:self.categFilter name:@""];
    
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateFetchNotif:) name:@"updateFetch" object:nil];
   
}

-(void)updateFetchNotif:(NSNotification *)notification
{
      visibleCell = NO;
    _fetchedResultsController = [baseManager fetchedResultsControllerOrganization:@"" name:@""];
    
    [organizTabl reloadData];
}

-(void)updateFetchWithFilter:(NSString *)filter categName:(NSString *)categName
{
    NSLog(@" FETCH %@",filter);
    _fetchedResultsController = [baseManager fetchedResultsControllerOrganization:categName name:filter];
    
    [organizTabl reloadData];
    [self refreshData];
}

-(void)updateFetch:(NSString *)categName
{
    _fetchedResultsController = [baseManager fetchedResultsControllerOrganization:categName name:@""];
    [organizTabl reloadData];
    [self fullRefresData];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)animCurrentCell:(int)cellIndex
{
    
    if (countCell==0) {
        return;
    }
    UITableView *tableView = organizTabl; // Or however you get your table view
    NSIndexPath *path = [[tableView indexPathsForVisibleRows] objectAtIndex:cellIndex];
    __block int newIndex = cellIndex+1;
    NSTimeInterval delayInSeconds;
    
    
    if (cellIndex == 0)
        delayInSeconds = 0;
    else
    delayInSeconds = 0.08;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [((OrganizCell *)[tableView cellForRowAtIndexPath:path]) animatedShow] ;
        if (newIndex < [[tableView indexPathsForVisibleRows] count])
            [self animCurrentCell:newIndex];
        
    });
    
    
}
-(void)fullRefresData
{
    visibleCell = NO;
    [organizTabl reloadData];
    
    
    NSTimeInterval delayInSeconds = 0.0;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        
        [self refreshData];
        
    });
    
}
-(void)refreshData
{
    
    if (!visibleCell) {
        visibleCell = YES;
        
        [self animCurrentCell:0];
    }
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id  sectionInfo =
    [[_fetchedResultsController sections] objectAtIndex:section];
    NSLog(@"CNT2 %lu",(unsigned long)[sectionInfo numberOfObjects]);
    countCell = (int)[sectionInfo numberOfObjects];
    return countCell;
}


- (void)configureCell:(OrganizCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    if (visibleCell) {
        [cell.categName setAlpha:1];
        [cell.adresName setAlpha:1];
    } else
    {
        [cell.categName setAlpha:0];
        [cell.adresName setAlpha:0];
    }
    NSManagedObject *org = [_fetchedResultsController objectAtIndexPath:indexPath];
    
   
    if ([((NSNumber *)[org valueForKey:@"order"]) integerValue]>0  && [((NSNumber *)[org valueForKey:@"order"]) integerValue] !=10000 && self.isSelected) {
        UIFont *currentFont = cell.categName.font;
        UIFont *newFont = [UIFont fontWithName:[NSString stringWithFormat:@"%@-Bold",currentFont.fontName] size:currentFont.pointSize];
        cell.categName .font = newFont;
        
        
        UIFont *currentFont2 = cell.adresName.font;
        UIFont *newFont2 = [UIFont fontWithName:[NSString stringWithFormat:@"%@-Bold",currentFont2.fontName] size:currentFont2.pointSize];
        cell.adresName .font = newFont2;
        
        
        
        if (IS_OS_7_OR_LATER)
        {
            UIGraphicsBeginImageContext(cell.contentView .frame.size);
            [[UIImage imageNamed:@"select_bg.png"] drawInRect:cell.contentView .bounds];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            cell.contentView .backgroundColor = [UIColor colorWithPatternImage:image];
        } else
        [cell.contentView setBackgroundColor:[UIColor colorWithRed:(252.0f/255.0f) green:(185.0f / 255.0f) blue:(4.0f / 255.0f) alpha:1]];
    }
    
    
    
    [cell.categName setText:[org valueForKey:@"name"]];
    [cell.adresName setText:[org valueForKey:@"adress"]];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"OrganizCell_%@",[[MySingleton sharedMySingleton] getSystemVersion]];
    
    
    
    OrganizCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        
        [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"OrganizCell_%@",[[MySingleton sharedMySingleton] getSystemVersion]] owner:self options:nil];
		cell = organizCell;
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    

    
  
    return cell;
    
}

#pragma mark - Table view delegate

//- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"account_cell_background_guide_selected.png"]];
//    UILabel *titleLabel = (UILabel*)[[[self.tableView cellForRowAtIndexPath:indexPath] contentView] viewWithTag:10];
//
//    titleLabel.textColor = [UIColor whiteColor];
//    titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
//    return indexPath;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
NSDictionary *titleInfo = [[NSDictionary alloc] initWithObjectsAndKeys:_fetchedResultsController,@"fetch", indexPath,@"indexPath",nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openInformation" object:titleInfo userInfo:nil];
   
    
    
}



#pragma mark NSFetchRequest

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [organizTabl beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = organizTabl;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(OrganizCell  *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [organizTabl insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [organizTabl deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [organizTabl endUpdates];
}


@end
