//
//  MapController.m
//  SpravkaMagadan
//
//  Created by Oleg on 27.12.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "MapController.h"
#import "Coordinates.h"


@interface Pin : NSObject
@property (assign) GLMapPoint pos;
@property (assign) int imageID;
@end

@implementation Pin
@end

@interface MapController ()
{
   
    int stepSelected;
     GLMapImage *_mapImage;
    GLMapImage *_mapImageMyLocation;
    NSMutableArray *pointArray;
    double minLat,maxLat,minLong,maxLong;
    BOOL Groupped;
}

@end

@implementation MapController
@synthesize mapView;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize latitude= _latitude;
@synthesize longitude=_longitude;
@synthesize selectTitle = _selectTitle;
@synthesize coordinatesArray = _coordinatesArray;





- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  //  _mapView.showUserLocation = YES;
  
    baseManager = [[BaseManager alloc] init];
    NSError *error;
    if (![[baseManager fetchedResultsControllerCategory:@"" full:YES] performFetch:&error]) {
        // Update to handle the error appropriately.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        exit(-1);  // Fail
        
    }
    _fetchedResultsController = [baseManager fetchedResultsControllerAdress:@""];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateFetch:) name:@"updateFetch" object:nil];
    // Do any additional setup after loading the view.
    
    _latitude = 0;
    _longitude = 0;
    [selectStreetHouse setAlpha:0];
   
    stepSelected = 0;
    
    [_selectTitle setText:@""];
    self.coordinatesArray = [[NSArray alloc] init];
    
   
    pointArray = [[NSMutableArray alloc] init];
    
}

-(void)updateFetch:(NSNotification *)notification
{
    
    //_fetchedResultsController = [baseManager fetchedResultsControllerCategory:@"" full:YES];
    [categTabl reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
   [self.mapView startRenderWithFrameInterval:0.5];
      [self loadEmbedMapWitCoordinates];
    

   
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [selectStreetHouse setFrame:self.view.frame];
    [self.view addSubview:selectStreetHouse];
}

// Stop rendering when map is hidden to save resources on CADisplayLink calls
-(void)viewDidDisappear:(BOOL)animated
{
    [self.mapView stopRender];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Control Action

-(IBAction)currentLocation:(id)sender
{
    [self loadEmbedMapWithCurrentLocation];
}
-(IBAction)searchByAdress:(id)sender
{
    stepSelected = 0;
     _fetchedResultsController = [baseManager fetchedResultsControllerAdress:@""];
    [categTabl reloadData];
            [self showHideSelectView];
    
}
-(IBAction)posToSelected:(id)sender
{
    
    if (Groupped) {
        [self loadImageGroup];
        return;
    }
    if (_latitude>0 && _longitude>0) {
        [self loadEmbedMapWitCoordinates];
    }
}


-(IBAction)backToMain:(id)sender
{
    [mapView removeImageGroup:_mapImageGroup];
    
    _mapImageGroup = nil;
    if (self.FromInfo) {
        [self.view removeFromSuperview];
    } else
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goToMainCateg" object:nil userInfo:nil];
    
}


-(IBAction)closeSelectView:(id)sender
{
    [self showHideSelectView];
}
#pragma mark Inner Logic

-(void)showHideSelectView
{
    int currentAlpha = selectStreetHouse.alpha;
    if (currentAlpha == 0) {
        currentAlpha = 1;
    } else
        currentAlpha = 0;
    
    [ UIView animateWithDuration:0.1 animations:^{
        
        [selectStreetHouse setAlpha:currentAlpha];
    } completion:^(BOOL finished) {
        
    }];
}

- (void) zoomToBBox {
    // Berlin
    CGPoint geoPt1 = CGPointMake(150.7938821, 59.58034652);
    // Minsk
    CGPoint geoPt2 = CGPointMake(150.82821442, 59.55456466);
    
    // get internal coordinates of geo points
    GLMapPoint internalPt1 = [GLMapView convertGeoToInternal:geoPt1];
    GLMapPoint internalPt2 = [GLMapView convertGeoToInternal:geoPt2];
    
    // get pixel positions of geo points
    CGPoint screenPt1 = [self.mapView convertInternalToDisplay:internalPt1];
    CGPoint screenPt2 = [self.mapView convertInternalToDisplay:internalPt2];
    
    // get distance in pixels in current zoom level
    CGPoint screenDistance = CGPointMake(fabs(screenPt1.x - screenPt2.x), fabs(screenPt1.y - screenPt2.y));
    
    // get scale beteen current screen size and desired screen size to fit points
    double wscale = screenDistance.x / self.mapView.bounds.size.width;
    double hscale = screenDistance.y / self.mapView.bounds.size.height;
    double zoomChange = fmax(wscale, hscale);
    
    // set center point
    [self.mapView setMapCenter:GLMapPointMake((internalPt1.x + internalPt2.x)/2, (internalPt1.y + internalPt2.y)/2) animated:NO];
    // change zoom to make screenDistance less or equal mapView.bounds
    [self.mapView setMapZoom:[self.mapView mapZoom]/zoomChange animated:NO];
}


-(void)loadImageGroup
{
    
    Groupped = YES;
    [pointArray removeAllObjects];
    
    [[GLMapManager manager] addMapWithPath:[[NSBundle mainBundle] pathForResource:@"magadan" ofType:@"vm"]];
    
    GLMapPoint pos = [GLMapView convertGeoToInternal:CGPointMake(150.79353882, 59.57059355)];
  //  [self.mapView setMapZoom:1<<14 animated:YES];
 //   [self.mapView setMapCenter:pos animated:NO];
    if(!_mapImageGroup)
    {
        _mapImageGroup = [self.mapView createImageGroup];
        
        NSArray *images = @[[UIImage imageNamed:@"marker_new.png"]];
        
        __weak MapController *bself = self;
        _imageIDs = [_mapImageGroup setImages:images complete:^{
            for(int i=0; i<[images count]; i++)
            {
                UIImage *img = images[i];
                [bself.mapImageGroup setImageOffset:CGPointMake(img.size.width/2, 0) forImageWithID:[bself.imageIDs[i] intValue]];
            }
        }];
        
        [_mapImageGroup setObjectFillBlock:^(size_t index, uint32_t *imageID, GLMapPoint *pos)
         {
             if (bself.pins && [bself.pins count] > index) {
                 Pin *pin = [bself.pins objectAtIndex:index];
                 *pos = pin.pos;
                 *imageID = pin.imageID;
             }
         }];
        
        _pins = [[NSMutableArray alloc] initWithCapacity:1];
    }
    
    minLat = [[[self.coordinatesArray objectAtIndex:0] objectForKey:@"latitude"] doubleValue];
    maxLat = [[[self.coordinatesArray objectAtIndex:0] objectForKey:@"latitude"] doubleValue];
    minLong = [[[self.coordinatesArray objectAtIndex:0] objectForKey:@"longitude"] doubleValue];
    maxLong = [[[self.coordinatesArray objectAtIndex:0] objectForKey:@"longitude"] doubleValue];
    
    for (NSDictionary *dict in self.coordinatesArray) {
        
        NSLog(@"dic %@",[dict description]);
        Pin *pin = [[Pin alloc] init];
        //    mapController.latitude = [[[aNotification.object objectAtIndex:0] objectForKey:@"latitude"] doubleValue];
        //    mapController.longitude = [[[aNotification.object objectAtIndex:0] objectForKey:@"longitude"] doubleValue];
        
        if ([[dict objectForKey:@"latitude"] doubleValue] < minLat) {
            minLat =[[dict objectForKey:@"latitude"] doubleValue];
        }
        
        if ([[dict objectForKey:@"longitude"] doubleValue] < minLong) {
            minLong =[[dict objectForKey:@"longitude"] doubleValue];
        }
        
        if ([[dict objectForKey:@"latitude"] doubleValue] >maxLat) {
            maxLat =[[dict objectForKey:@"latitude"] doubleValue];
        }
        
        if ([[dict objectForKey:@"longitude"] doubleValue] > maxLong) {
            maxLong =[[dict objectForKey:@"longitude"] doubleValue];
        }
        
        CGPoint testPoint = CGPointMake([[dict objectForKey:@"longitude"] doubleValue], [[dict objectForKey:@"latitude"] doubleValue]);
        
        [pointArray addObject:[NSValue valueWithCGPoint:testPoint]];
        GLMapPoint posM = [GLMapView convertGeoToInternal:CGPointMake([[dict objectForKey:@"longitude"] doubleValue] , [[dict objectForKey:@"latitude"] doubleValue])];
        pin.pos = posM;
        
        // to iterate over images pin1, pin2, pin3, pin1, pin2, pin3
        // NSUInteger imageIDindex = [_pins count] % [_imageIDs count];
        pin.imageID = [_imageIDs[0] intValue];
        [_pins addObject:pin];
    }
    
    
    
    [_mapImageGroup setObjectCount:[_pins count]];
    [_mapImageGroup setNeedsUpdate];
    
    [self zoomToRect];
}


-(void)zoomToRect
{
    CGPoint geoPt1 = CGPointMake(minLong-0.005, minLat-0.005);
    // Minsk
    CGPoint geoPt2 = CGPointMake(maxLong+0.005, maxLat+0.005);
    
    // get internal coordinates of geo points
    GLMapPoint internalPt1 = [GLMapView convertGeoToInternal:geoPt1];
    GLMapPoint internalPt2 = [GLMapView convertGeoToInternal:geoPt2];
    
    // get pixel positions of geo points
    CGPoint screenPt1 = [self.mapView convertInternalToDisplay:internalPt1];
    CGPoint screenPt2 = [self.mapView convertInternalToDisplay:internalPt2];
    
    // get distance in pixels in current zoom level
    CGPoint screenDistance = CGPointMake(fabs(screenPt1.x - screenPt2.x), fabs(screenPt1.y - screenPt2.y));
    
    // get scale beteen current screen size and desired screen size to fit points
    double wscale = screenDistance.x / self.mapView.bounds.size.width;
    double hscale = screenDistance.y / self.mapView.bounds.size.height;
    double zoomChange = fmax(wscale, hscale);
    
    // set center point
    [self.mapView setMapCenter:GLMapPointMake((internalPt1.x + internalPt2.x)/2, (internalPt1.y + internalPt2.y)/2) animated:NO];
    // change zoom to make screenDistance less or equal mapView.bounds
    [self.mapView setMapZoom:[self.mapView mapZoom]/zoomChange animated:YES];

   
}

- (void) loadEmbedMap {
    Groupped = NO;
    [[GLMapManager manager] addMapWithPath:[[NSBundle mainBundle] pathForResource:@"magadan" ofType:@"vm"]];

   GLMapPoint pos = [GLMapView convertGeoToInternal:CGPointMake(150.79353882, 59.57059355)];
    [self.mapView setMapZoom:1<<14 animated:YES];
    [self.mapView setMapCenter:pos animated:NO];
    
    if (_latitude == 0 && _latitude ==0) {
        
    } else
    {
        [self loadEmbedMapWitCoordinates];
        [self posToSelected:nil];
        
    }
}


- (void) loadEmbedMapWithCurrentLocation {
  //  Groupped = NO;
    GLMapPoint pos = [GLMapView convertGeoToInternal:CGPointMake([[MySingleton sharedMySingleton] getCurrentLocation].coordinate.longitude , [[MySingleton sharedMySingleton] getCurrentLocation].coordinate.latitude)];
    [self.mapView setMapZoom:1<<18 animated:YES];
    [self.mapView setMapCenter:pos animated:NO];
    UIImage *img = [UIImage imageNamed:@"marker_my.png"];
    [mapView removeImage:_mapImageMyLocation];
    _mapImageMyLocation = [mapView displayImage:img];
    
    _mapImageMyLocation.offset = CGPointMake(img.size.width/2, img.size.height);
    _mapImageMyLocation.position = mapView.mapCenter;
  
    _mapImageMyLocation.angle = 180;
    _mapImageMyLocation.rotatesWithMap = NO;
}
- (void) loadEmbedMapWitCoordinates {
    
    
    Groupped = NO;
    NSLog(@"%f %f",_latitude, _longitude);
    GLMapPoint pos = [GLMapView convertGeoToInternal:CGPointMake(_longitude , _latitude)];
    [self.mapView setMapZoom:1<<18 animated:NO];
    [self.mapView setMapCenter:pos animated:NO];
    UIImage *img = [UIImage imageNamed:@"marker_new.png"];
    [mapView removeImage:_mapImage];
    [mapView removeImageGroup:_mapImageGroup];
    _mapImage = [mapView displayImage:img];
    _mapImage.offset = CGPointMake(img.size.width/2, img.size.height);
    _mapImage.position = mapView.mapCenter;
   
   _mapImage.angle = 180;
    _mapImage.rotatesWithMap = NO;
    //[self loadEmbedMapWitCoordinates2];
    
}

- (void) loadEmbedMapWitCoordinates2 {
    Groupped = NO;
    GLMapPoint pos = [GLMapView convertGeoToInternal:CGPointMake(_longitude , _latitude)];
    [self.mapView setMapZoom:1<<18 animated:NO];
    [self.mapView setMapCenter:pos animated:NO];
    UIImage *img = [UIImage imageNamed:@"marker_new.png"];
    [mapView removeImage:_mapImage];
    _mapImage = [mapView displayImage:img];
    _mapImage.offset = CGPointMake(img.size.width/2, img.size.height);
    _mapImage.position = mapView.mapCenter;
    
    _mapImage.angle = 180;
    _mapImage.rotatesWithMap = NO;
 
    
}
#pragma mark - Table view data source








- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return countCell;    // Return the number of rows in the section.
    id  sectionInfo =
    [[_fetchedResultsController sections] objectAtIndex:section];
    
    countCell = (int)[sectionInfo numberOfObjects];
    return countCell;
}


- (void)configureCell:(CategCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
      
    NSManagedObject *org = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *key;
    
    if (stepSelected == 0) {
        key = [NSString stringWithFormat:@"%@",@"street"];
    } else
    {
        key = [NSString stringWithFormat:@"%@",@"home"];
        if ([[org valueForKey:key] isEqualToString:@"---"]) {
            _latitude = [[org valueForKey:@"latitude"] doubleValue];
            _longitude = [[org valueForKey:@"longitude"] doubleValue];
            
            [_selectTitle setText:[NSString stringWithFormat:@"%@, %@",[org valueForKey:@"street"],[org valueForKey:@"home"]]];
            
            NSLog(@"%@",[org description]);
            stepSelected = 0;
            [self loadEmbedMapWitCoordinates];
            [self showHideSelectView];
            
        }
        
        
    }
    
  
    
    [cell.categName setText:[org valueForKey:key]];
    

    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"CategCell_%@",[[MySingleton sharedMySingleton] getSystemVersion]];
    
    
    
    CategCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        
        [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"CategCell_%@",[[MySingleton sharedMySingleton] getSystemVersion]] owner:self options:nil];
        cell = categCell;
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    
    return cell;
    
}

#pragma mark - Table view delegate

//- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"account_cell_background_guide_selected.png"]];
//    UILabel *titleLabel = (UILabel*)[[[self.tableView cellForRowAtIndexPath:indexPath] contentView] viewWithTag:10];
//
//    titleLabel.textColor = [UIColor whiteColor];
//    titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
//    return indexPath;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategCell *cell = (CategCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (stepSelected == 0) {
        stepSelected = 1;
        _fetchedResultsController = [baseManager fetchedResultsControllerAdress:cell.categName.text];
        NSLog(@"%lu - ",(unsigned long)[_fetchedResultsController.fetchedObjects count]);
        [categTabl reloadData];
        
//         Coordinates *org = [_fetchedResultsController objectAtIndexPath:indexPath];
//        
//         NSString *key;
//        key =[NSString stringWithFormat:@"%@",@"home"];
//        if ([org valueForKey:key] != nil) {
//          
//        } else
//        {
//                _latitude = [[org valueForKey:@"latitude"] doubleValue];
//                _longitude = [[org valueForKey:@"longitude"] doubleValue];
//                
//                [_selectTitle setText:[NSString stringWithFormat:@"%@, %@",[org valueForKey:@"street"],[org valueForKey:@"home"]]];
//                
//                NSLog(@"%@",[org description]);
//                stepSelected = 0;
//                [self loadEmbedMapWitCoordinates];
//                [self showHideSelectView];
//            
//            
//        }
        
        
    }else
    {
        NSManagedObject *org = [_fetchedResultsController objectAtIndexPath:indexPath];

        _latitude = [[org valueForKey:@"latitude"] doubleValue];
        _longitude = [[org valueForKey:@"longitude"] doubleValue];
        
        [_selectTitle setText:[NSString stringWithFormat:@"%@, %@",[org valueForKey:@"street"],[org valueForKey:@"home"]]];
        
        NSLog(@"%@",[org description]);
        stepSelected = 0;
        [self loadEmbedMapWitCoordinates];
        [self showHideSelectView];
        
    }
  

    
}


#pragma mark NSFetchRequest

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    NSLog(@"1");
    [categTabl beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = categTabl;
    NSLog(@"2");
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(CategCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    NSLog(@"3");
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [categTabl insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [categTabl deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    NSLog(@"4");
    [categTabl endUpdates];
}

@end
