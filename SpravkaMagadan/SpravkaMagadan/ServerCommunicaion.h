//
//  ServerCommunicaion.h
//  SpravkaMagadan
//
//  Created by gelgard on 04.05.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServerCommunicaion : NSObject



+(void)downloadAndUnZip:(ServerFileType)fileType downloadedFilePath:(void (^)(NSString* fileDownloadedName))fileDownloaded;
@end
