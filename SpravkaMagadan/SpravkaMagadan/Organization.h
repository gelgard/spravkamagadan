//
//  Organization.h
//  SpravkaMagadan
//
//  Created by gelgard on 06.05.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Organization : NSManagedObject

@property (nonatomic, retain) NSString * categ;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * adress;
@property (nonatomic, retain) NSString * info;
@property (nonatomic, retain) NSString * phone_1;
@property (nonatomic, retain) NSString * coment_1;
@property (nonatomic, retain) NSString * phone_2;
@property (nonatomic, retain) NSString * phone_3;
@property (nonatomic, retain) NSString * phone_4;
@property (nonatomic, retain) NSString * phone_5;
@property (nonatomic, retain) NSString * phone_6;
@property (nonatomic, retain) NSString * phone_7;
@property (nonatomic, retain) NSString * phone_8;
@property (nonatomic, retain) NSString * phone_9;
@property (nonatomic, retain) NSString * phone_10;
@property (nonatomic, retain) NSString * coment_2;
@property (nonatomic, retain) NSString * coment_3;
@property (nonatomic, retain) NSString * coment_4;
@property (nonatomic, retain) NSString * coment_5;
@property (nonatomic, retain) NSString * coment_6;
@property (nonatomic, retain) NSString * coment_7;
@property (nonatomic, retain) NSString * coment_8;
@property (nonatomic, retain) NSString * coment_9;
@property (nonatomic, retain) NSString * coment_10;
@property (nonatomic, retain) NSString * iconlink;
@property (nonatomic, retain) NSString * bold;
@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) NSNumber * maincategorder;

@end
