//
//  InfoTableController.m
//  SpravkaMagadan
//
//  Created by gelgard on 11.05.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "InfoTableController.h"

@interface InfoTableController ()

@end

@implementation InfoTableController
{
    
}
@synthesize infoTable, nameAdress, phones, selArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (IS_OS_7_OR_LATER) {
        [self.infoTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    nameAdress = [[NSMutableArray alloc] init];
    phones = [[NSMutableArray alloc] init];
    self.selArray = [[NSMutableArray alloc] init];
    
    mapView = [[MapController alloc] init];
    [mapView.view setFrame:self.view.bounds];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
//    if (self.selfFrame.origin.x >0) {
//        [infoTable setFrame:self.selfFrame];
//    }
//    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    self.selfFrame = self.view.frame;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"HomeInfoCell";
    
        HomeInfoCell *cell = (HomeInfoCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
        if (cell == nil)
        {
           
        }
   
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeInfoCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    
    NSString *suff;
    
    if (IS_OS_7_OR_LATER) {
        suff = @"7";
    } else
        suff = @"6";
    
    int adresCount =(int)[self.nameAdress count] -1 ;
    
    [cell.categViews setAlpha:0];
    
    if (indexPath.row > adresCount) {
        
        [cell.phoneView setAlpha:1];
        [cell.adresView setAlpha:0];
        
        [cell.phoneLabel setText:[[self.phones objectAtIndex:indexPath.row-[self.nameAdress count]] objectForKey:@"phone"]];
        
        
        if ([[[self.phones objectAtIndex:indexPath.row-[self.nameAdress count]] objectForKey:@"comment"] length]>0) {
            [cell.phoneLabel setFrame:CGRectMake(3, 5, 237, 35)];
        }else
            [cell.phoneLabel setFrame:CGRectMake(3, cell.phoneView.bounds.size.height/2-cell.phoneLabel.bounds.size.height / 2, 237, 35)];
        [cell.adresPhoneLabel setText:[[self.phones objectAtIndex:indexPath.row-[self.nameAdress count]] objectForKey:@"comment"]];

        [cell.imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"phone_tmb_%@.png", suff]]];
        
    } else
    {
        [cell.moreLabel setAlpha:0];
        cell.nameLabel.numberOfLines = 2;
         [cell.nameLabel setFrame:CGRectMake(0, 8, 225, 42)];
        
        

        
        if (adresCount <=1) {
            if (indexPath.row == 0) {
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell.nameLabel setText:(NSString *)[self.nameAdress objectAtIndex:0]];
                
                [cell.imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"home_tmb_%@.png", suff]]];
            }
        } else
        {
            
            switch (indexPath.row) {
                case 0:
                {
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell.nameLabel setText:(NSString *)[self.nameAdress objectAtIndex:0]];
                    
                    [cell.imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"home_tmb_%@.png", suff]]];
                }
                    break;
                case 1:
                {
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell.nameLabel setText:(NSString *)[self.nameAdress objectAtIndex:1]];
                    
                    //  [cell.imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"adres_tmb_%@.png", suff]]];
                    [cell.imageView setImage:[UIImage imageNamed:@"info.png"]];
                    
                    [cell.moreLabel setAlpha:[self isCellExapndable:cell.nameLabel]];
                    int nH = [self isCellExpanded:indexPath];
                    if (nH>0) {
                        [cell.nameLabel setFrame:CGRectMake(0, 8, 225, nH)];
                        cell.nameLabel.numberOfLines = 0;
                        [cell.moreLabel setAlpha:0];
                        [cell.nameLabel sizeToFit];
                    } else
                    {
                        [cell.nameLabel setFrame:CGRectMake(0, 8, 225, 42)];
                        cell.nameLabel.numberOfLines = 2;
                        
                        [cell.nameLabel sizeToFit];
                    }
                }
                    break;

                case 2:
                {
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    //[cell.nameLabel setText:[[self.nameAdress objectAtIndex:2] description]];
                    [cell.nameLabel setAlpha:0];
                      [cell.imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"adres_tmb_%@.png", suff]]];
                    [cell generateCategView:[self.nameAdress objectAtIndex:2]];
                    
                    [cell.moreLabel setAlpha:[self isCellExapndableTextView:cell.categViews]];
                    int nH = [self isCellExpanded:indexPath];
                    if (nH>0) {
                        [cell.categViews setFrame:CGRectMake(60, 0, 225, nH)];
                    
                        [cell.moreLabel setAlpha:0];
                        
                    } else
                    {
                        [cell.categViews setFrame:CGRectMake(60, 0, 225, 38)];
                                         }
                    
                    [cell generateCategView:[self.nameAdress objectAtIndex:2]];
                }
                    break;
                    
                case 3:
                {
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell.nameLabel setText:@"Показать на карте"];
                    
                    [cell.imageView setImage:[UIImage imageNamed:@"marker_new.png"]];
                    
                 //   [cell generateCategView:[self.nameAdress objectAtIndex:2]];
                    
                }
                    break;

                default:
                    break;
            }
            
           
            NSLog(@"%@",cell.nameLabel.text);
            
           
        }
        
        
        if ([tableView cellForRowAtIndexPath:indexPath].contentView.frame.size.height >65) {
            NSLog(@"%@",cell.nameLabel.text);
        }
        [cell.phoneView setAlpha:0];
        [cell.adresView setAlpha:1];
           }
    
    //[cell.contentView setBackgroundColor:[UIColor grayColor]];
    
//        cell.nameLabel.text = [tableData objectAtIndex:indexPath.row];
//        cell.thumbnailImageView.image = [UIImage imageNamed:[thumbnails objectAtIndex:indexPath.row]];
//        cell.prepTimeLabel.text = [prepTime objectAtIndex:indexPath.row];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int nH;
    nH = [self isCellExpanded:indexPath];
        if (nH >0) {
            
            if (indexPath.row == 2) {
                 return nH - 38+65;
            }
            
            if (indexPath.row == 1) {
                return nH - 42+65;
            }
           
        }
    
    
    return 65;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    int count = [self.nameAdress count]+[self.phones count];
    return count;
}
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row > [self.nameAdress count] -1)
    {
        return indexPath;
    }
    

//    } else
//    {
//        ((HomeInfoCell *)[tableView cellForRowAtIndexPath:indexPath]).selectionStyle = UITableViewCellSelectionStyleNone;
//        return nil;
//    }
    return indexPath;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (indexPath.row > [self.nameAdress count] -1)
    {
        NSString *phoneNumber = [@"tel://" stringByAppendingString:((HomeInfoCell *)[tableView cellForRowAtIndexPath:indexPath]).phoneLabel.text];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    } else
    {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
        if ([self isCellExapndable:((HomeInfoCell *)[tableView cellForRowAtIndexPath:indexPath]).nameLabel]) {
            NSLog(@"EXPAND");
            
            if (![self isCellExpanded:indexPath]) {
                
                
                if (indexPath.row == 1) {
                    [self expanCollapseCell:(int)indexPath.row newHeight:[[MySingleton sharedMySingleton] heightFromString:((HomeInfoCell *)[tableView cellForRowAtIndexPath:indexPath]).nameLabel.text withFont:((HomeInfoCell *)[tableView cellForRowAtIndexPath:indexPath]).nameLabel.font constraintToWidth:((HomeInfoCell *)[tableView cellForRowAtIndexPath:indexPath]).nameLabel.bounds.size.width]];
                }
                if (indexPath.row == 2) {
                    [self expanCollapseCell:(int)indexPath.row newHeight:([[MySingleton sharedMySingleton] heightFromString:((HomeInfoCell *)[tableView cellForRowAtIndexPath:indexPath]).categViews.text withFont:((HomeInfoCell *)[tableView cellForRowAtIndexPath:indexPath]).categViews.font constraintToWidth:((HomeInfoCell *)[tableView cellForRowAtIndexPath:indexPath]).categViews.bounds.size.width]+8)];
                }
                
                [self.infoTable reloadData];
            }
            
        }
        
        
        if ([[nameAdress objectAtIndex:indexPath.row] isKindOfClass:[NSMutableArray class]] && indexPath.row > 2) {

            NSLog(@"%@", [[nameAdress objectAtIndex:indexPath.row] description]);
            
               [[NSNotificationCenter defaultCenter] postNotificationName:@"openMap" object:[nameAdress objectAtIndex:indexPath.row] userInfo:nil];
   
        }

    }
    
}

-(int)isCellExpanded:(NSIndexPath *)indexPath
{
    int res;
    res = 0;
    NSLog(@" %@ - %i", self.selArray, indexPath.row);
    for (NSDictionary *selC in self.selArray) {
        NSLog(@"%i %i",(int)indexPath.row,[[selC objectForKey:@"index"] intValue]);
        if ((int)indexPath.row == [[selC objectForKey:@"index"] intValue]) {
            res = [[selC objectForKey:@"height"] intValue];
        }
    }
    
    return res;
}

-(void)expanCollapseCell:(int)cellIndex newHeight:(int)newHeight
{
    BOOL add;
    add = YES;
    NSLog(@"%i",newHeight);
    for (NSDictionary *selC in self.selArray) {
        if (cellIndex == [[selC objectForKey:@"index"] intValue]) {
            [self.selArray removeObjectAtIndex:cellIndex];
            add = NO;
        }
    }
    
    if (add) {
       
        [self.selArray addObject:[NSDictionary dictionaryWithObjects:@[[NSNumber numberWithInt:cellIndex],[NSNumber numberWithInt:newHeight]] forKeys:@[@"index",@"height"]]];
        
    }
}

#pragma mark Expandable

-(BOOL)isCellExapndable:(UILabel *)infoLabl
{
  
    int newHeight;
    newHeight = [[MySingleton sharedMySingleton] heightFromString:infoLabl.text withFont:infoLabl.font constraintToWidth:infoLabl.bounds.size.width];
    
    NSLog(@"new height %i", newHeight);
    return newHeight > 45;
    
}


-(BOOL)isCellExapndableTextView:(UITextView *)infoLabl
{
    
    int newHeight;
    newHeight = [[MySingleton sharedMySingleton] heightFromString:infoLabl.text withFont:infoLabl.font constraintToWidth:infoLabl.bounds.size.width];
    
    NSLog(@"new height %i", newHeight);
    return newHeight > 36;
    
}
@end
