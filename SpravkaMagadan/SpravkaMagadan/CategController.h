//
//  CategController.h
//  SpravkaMagadan
//
//  Created by gelgard on 26.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PanelDelegate.h"
#import "TopPanelDelegate.h"
#import "EmptyInformationController.h"
#import "InfoTableController.h"

@interface CategController : UIViewController <UIScrollViewDelegate>
{
    BOOL pageControlUsed;
    int scrollHeight;
     NSArray *mainTitles;
    NSArray *placeholders;
    IBOutlet UIView *informationView;
    IBOutlet UILabel *infoName;
    int prevPage;
    UIViewController *emptyView;
    int currentIndex;
    NSFetchedResultsController *fetchResult;
    NSIndexPath *currentIndexPath;
    InfoTableController *infoTableView;
}


- (void)loadScrollViewWithPage:(int)page;
- (void)scrollViewDidScroll:(UIScrollView *)sender;
-(void)showInfo:(NSIndexPath *)currentRow;
//-(void)updateFetchWithFilter:(NSString *)filter;

@property (nonatomic, strong) IBOutlet UILabel *mainTitle;
@property (nonatomic, strong) IBOutlet UIButton *navButton;
@property (nonatomic, assign) id<PanelDelegate> delegate;
@property (nonatomic, assign) id<TopPanelDelegate> topDelegate;
@property (nonatomic, strong) IBOutlet UIPageControl *pageController;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *viewControllers;
@property (nonatomic, strong) IBOutlet UITextField *searchField;
@property (nonatomic, strong) IBOutlet UIView *infoTable;

-(IBAction)navBtnClk:(id)sender;
-(IBAction)closeInfoView:(id)sender;

-(IBAction)searchStart:(id)sender;
-(IBAction)cancelClick:(id)sender;
-(IBAction)navigateByOrganiz:(id)sender;
@end
