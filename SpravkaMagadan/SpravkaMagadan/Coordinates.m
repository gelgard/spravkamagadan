//
//  Coordinates.m
//  SpravkaMagadan
//
//  Created by Oleg on 01.03.15.
//  Copyright (c) 2015 uran. All rights reserved.
//

#import "Coordinates.h"


@implementation Coordinates

@dynamic home;
@dynamic latitude;
@dynamic longitude;
@dynamic street;
@dynamic ord;

@end
