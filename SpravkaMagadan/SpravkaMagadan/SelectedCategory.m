//
//  SelectedCategory.m
//  SpravkaMagadan
//
//  Created by gelgard on 27.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "SelectedCategory.h"

@interface SelectedCategory ()

@end

@implementation SelectedCategory

@synthesize categTitle, placeholderText;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark MyAction

-(IBAction)searchStart:(id)sender
{
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    notUpdate = NO;
    // Do any additional setup after loading the view.
}

-(void)onKeyboardHide:(NSNotification *)notification
{
    //keyboard will hide
    
    if ([self.searchTex.text length]>0) {
        return;
    }
    [scrollViewPlacehoder setAlpha:1];
    [self.placeHolderLabel setFrame:CGRectMake(0, 0, 300, 30)];
    self.searchTex.placeholder = @"";
    //[self.searchTex becomeFirstResponder];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    BOOL isTruncated = NO;
    
    CGSize size = [self.categTitle.text sizeWithFont:self.categTitle.font];
    NSLog(@"width %f",size.width);
    if (size.width > self.categTitle.bounds.size.width) {
        isTruncated = YES;
    }
    if (isTruncated) {
        NSLog(@"ZAPUSKAIIII ");
        CGRect labelWidth = self.categTitle.frame;
        labelWidth.size.width = size.width+40;
        
        [self.categTitle setFrame:labelWidth];
        
        myTimer2 = [NSTimer scheduledTimerWithTimeInterval:0.05f
                                                   target: self
                                                 selector:@selector(scrollText2:)
                                                 userInfo: nil
                                                  repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:myTimer2 forMode:NSRunLoopCommonModes];
        
        
        
    }
    if (proivodstvaController == nil)
    {
        NSString *nibName = [NSString stringWithFormat:@"CategScrollScreenView_2_%@",[[MySingleton sharedMySingleton] getSystemVersion]  ]  ;
        
        proivodstvaController = [[CategController_2 alloc] initWithNibName:nibName bundle:nil filter:self.categTitle.text];
        proivodstvaController.isSelected = YES;
       // [proivodstvaController.view setFrame:CGRectMake(0, 0, proizvodstvaView.frame.size.width, proizvodstvaView.frame.size.height)];
        [proizvodstvaView addSubview:proivodstvaController.view];
    }
    
    
    
    
    
    int scrollHeight;
    if (!IS_IPHONE5) {
        scrollHeight = 343;
    } else
        scrollHeight = 428;
   // [proizvodstvaView setFrame:CGRectMake(0, 127, 320, scrollHeight)];
    
    NSLog(@" %f %f",proizvodstvaView.frame.size.width,proizvodstvaView.frame.size.height);
    [proizvodstvaView layoutIfNeeded];
    [proivodstvaController.view setFrame:CGRectMake(0, 0, proizvodstvaView.frame.size.width, proizvodstvaView.frame.size.height)];
    
    if (!notUpdate)
    [proivodstvaController fullRefresData];
    notUpdate = YES;
    [myTimer invalidate];
    myTimer = [NSTimer scheduledTimerWithTimeInterval:0.05f
                                                    target: self
                                                  selector:@selector(scrollText:)
                                                  userInfo: nil
                                                   repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:myTimer forMode:NSRunLoopCommonModes];
    
    
}
-(void)scrollText2:(id)parameter{
    
    self.categTitle.center = CGPointMake(self.categTitle.center.x - 1.2, self.categTitle.center.y);
    
    if (self.categTitle.center.x < -(self.categTitle.bounds.size.width/2)){
        self.categTitle.center = CGPointMake(320 + (self.categTitle.bounds.size.width/1/5), self.categTitle.center.y);
    }
}

-(void)scrollText:(id)parameter{
    
    self.placeHolderLabel.center = CGPointMake(self.placeHolderLabel.center.x - 1.2, self.placeHolderLabel.center.y);
    
    if (self.placeHolderLabel.center.x < -(self.placeHolderLabel.bounds.size.width/1.5)){
        self.placeHolderLabel.center = CGPointMake(320 + (self.placeHolderLabel.bounds.size.width/1/5), self.placeHolderLabel.center.y);
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark MyAction 
- (BOOL)isLabelTruncated:(UILabel *)label
{
    BOOL isTruncated = NO;
    
    CGSize size = [label.text sizeWithFont:label.font];
    NSLog(@"width %f",size.width);
    if (size.width > label.bounds.size.width) {
        isTruncated = YES;
    }
    
    return isTruncated;
}
-(IBAction)cancelClick:(id)sender
{
    
    if([((UITextField *)sender).text isEqualToString:@""] || ((UITextField *)sender).text == nil)
        [(UITextField *)sender resignFirstResponder];
    
    
}
-(IBAction)backBtn:(id)sender
{
    notUpdate = NO;
    [self.view removeFromSuperview];
    [myTimer2 invalidate];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateAnimated" object:nil userInfo:nil];

}
-(IBAction)editStart:(id)sender
{
    
}

-(IBAction)clckOnPlaceholderScroll:(id)sender
{
    [scrollViewPlacehoder setAlpha:0];
    self.searchTex.placeholder = self.placeholderText;
    [self.searchTex becomeFirstResponder];
}

-(IBAction)searchGo:(id)sender
{
    NSLog(@"GO");
    
    if ([self.searchTex.text length]>0) {
        [scrollViewPlacehoder setAlpha:0];
    }
    
    NSString *tempVal;
    tempVal = ((UITextField *)sender).text;
    
    [proivodstvaController updateFetchWithFilter:self.searchTex.text categName:self.categTitle.text];
    
    
    if([((UITextField *)sender).text isEqualToString:@""] || ((UITextField *)sender).text == nil)
        [(UITextField *)sender resignFirstResponder];

}


@end
