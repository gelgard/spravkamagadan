//
//  TopPanel.m
//  SpravkaMagadan
//
//  Created by gelgard on 26.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "TopPanel.h"

@interface TopPanel ()

@end


@implementation TopPanel

@synthesize navButton, mainTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
  
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeTitle:) name:@"changeTitle" object:nil];
    // Do any additional setup after loading the view.
}

-(void)changeTitle: (NSNotification*)aNotification
{
    NSLog(@"%@", [aNotification.object objectForKey:@"title"]);
    [mainTitle setText:[aNotification.object objectForKey:@"title"]];
    self.searchField.placeholder = [aNotification.object objectForKey:@"placeholder"];
   
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(IBAction)navBtnClk:(id)sender
{
        
    [_delegate navButtonClicked:(int)self.navButton.tag];
    
    [self.searchField resignFirstResponder];
    
    if (self.navButton.tag==1) {
        self.navButton.tag = 0;
    } else
        self.navButton.tag = 1;
    
    
}
#pragma mark main title 

-(void)hideMainTitle
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:0.3]; //or lower than 0.1
    [self.mainTitle setAlpha:0];
    [UIView commitAnimations];
}


-(IBAction)searchGo:(id)sender
{
    
}

@end
