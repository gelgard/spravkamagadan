#import "PanelDelegate.h"

@interface NavigationViewController : UIViewController
{
    IBOutlet UIView* buttonView;
}

@property (nonatomic, assign) id<PanelDelegate> delegate;


- (IBAction)redButtonClicked:(id)sender;
- (IBAction)greenButtonClicked:(id)sender;
- (IBAction)blueButtonClicked:(id)sender;

-(IBAction)menuBtnClck:(id)sender;
-(IBAction)backBtnClck:(id)sender;
-(IBAction)updateBase:(id)sender;
@end
