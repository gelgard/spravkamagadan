#import "NavigationViewController.h"

@interface NavigationViewController ()

@end

@implementation NavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initingButtons];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)redButtonClicked:(id)sender {
    [self.delegate didSelectViewWithName:@"RedView"];
}

- (IBAction)greenButtonClicked:(id)sender {
    [self.delegate didSelectViewWithName:@"GreenView"];
}

- (IBAction)blueButtonClicked:(id)sender {
    [self.delegate didSelectViewWithName:@"BlueView"];
}


-(IBAction)menuBtnClck:(id)sender
{
    //[self.delegate movePanelToOriginalPosition];
    
     //[self.delegate didSelectViewWithName:[NSString stringWithFormat:@"AddOrganizationView%i",((UIButton *)sender).tag]];
    if (((UIButton *)sender).tag == 4) {
         [[NSNotificationCenter defaultCenter] postNotificationName:@"updateBase" object:nil userInfo:nil];
    } else
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openMenuPunkt" object:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:((UIButton *)sender).tag],@"btntag", nil] userInfo:nil];
    
    
   
    
}


-(IBAction)updateBase:(id)sender
{
     [[NSNotificationCenter defaultCenter] postNotificationName:@"updateBase" object:nil userInfo:nil];
}

-(IBAction)backBtnClck:(id)sender
{
    [self.delegate movePanelToOriginalPosition];
}
-(void)initingButtons
{
    
//    if (IS_OS_7_OR_LATER) {
//        [buttonView setFrame:CGRectMake(0, 100, 290, 322)];
//    }
    
    NSArray *titles = [[NSArray alloc] initWithObjects:@"ДОБАВИТЬ ОРГАНИЗАЦИЮ", @"ИЗМЕНИТЬ ДАННЫЕ ОБ ОРГАНИЗАЦИИ",@"СООБЩИТЬ ОБ ОШИБКАХ В СПРАВОЧНИКЕ",@"КНИГА ЖАЛОБ И ПРЕДЛОЖЕНИЙ",@"ОБНОВИТЬ БАЗУ",@"КАРТА МАГАДАНА",@"О ПРИЛОЖЕНИИ",nil];
    for (UIView *btn in [buttonView subviews]) {
        NSLog(@"%li",(long)btn.tag);
        NSString *imName;
        if (IS_OS_7_OR_LATER)
        {
            imName =[NSString stringWithFormat:@"menu_%li",(long)btn.tag];
        } else
           imName =[NSString stringWithFormat:@"menu6_%li",(long)btn.tag];
        
        
        UIImage *image = [UIImage imageNamed:imName];
        UIImageView *imageView;
        
        
        if (IS_OS_7_OR_LATER)
        {
           imageView  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 45, 47)];
        } else
           imageView  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 42, 40)];
        
        
        [imageView setImage:image];
        
        // Create the label and set its text
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(imageView.frame.size.width+10, 0, btn.frame.size.width-imageView.frame.size.width-10, btn.frame.size.height)];
        [label setText:[titles objectAtIndex:btn.tag]];
        [label setFont:[UIFont boldSystemFontOfSize:12]];
        [label setBackgroundColor:[UIColor clearColor]];
        label.numberOfLines = 2;
        label.textColor = [UIColor whiteColor];
        // Put it all together
        [btn addSubview:label];
        [btn addSubview:imageView];
    }
}


@end
