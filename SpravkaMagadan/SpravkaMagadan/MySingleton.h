//
//  MySingleton.h
//  offM
//
//  Created by apple on 02.09.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MyCLController.h"



#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
#define IS_OS_5_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0)
#define IS_OS_6_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define mainUrl @"http://magadan-pioner.ru/api/"
#define addOrg @"?api.addOrg"
#define editOrg @"?api.editOrg"
#define errorAp @"?api.error"
#define offers @"?api.offers"


#define serverBasePath @"https://dl.dropboxusercontent.com/s/9ofgwtfbafhm581/base.zip?dl=1&token_hash=AAH-KCkmVvvk6PUY-8BQW1c2qoQ8RtN2-7Ka6FN2uYD7ZQ"
#define csvFile @"base.csv"



#define serverVresionPath @"https://dl.dropboxusercontent.com/s/eniocwrr5c1eopr/version.txt?dl=1&token_hash=AAGZW8oJ6KM29_BVD2IIq4QZBEyYv5Jasyywa4pVfmLoQQ"
#define versionFileName @"version.txt"
//https://dl.dropboxusercontent.com/s/ln9j0fet7uwpp0m/сoordinats.csv?dl=0

#define serverCoordinatesPath @"http://magadan-pioner.ru/api/adres.zip"
#define aboutPath @"http://magadan-pioner.ru/api/about.html"
//#define serverCoordinatesPath @"https://dl.dropboxusercontent.com/s/zpz2vsswryqs7c9/%D1%81oordinats.zip?dl=0"

#define coordinatesFileName @"coordinates.csv"


typedef enum {
    AddOrganizationView,
	EditOrganizationView,
    SendErrorView,
    SupposeBookView,
    MapView,
    AboutView
} MenuPunkts;


typedef enum {
   BaseFile,
	VersionFile,
    CoordinatesFile
} ServerFileType;

@interface MySingleton : NSObject <MyCLControllerDelegate>
{
    MyCLController *locationController;
    CLLocation *currentLocation;

    
}



+(MySingleton*)sharedMySingleton;

-(NSString *)getSystemVersion;
-(CLLocation *)getCurrentLocation;
- (double)heightFromString:(NSString*)text withFont:(UIFont*)font constraintToWidth:(double)widthL;


@end