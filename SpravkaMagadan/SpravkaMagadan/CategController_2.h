//
//  CategController_2.h
//  SpravkaMagadan
//
//  Created by gelgard on 26.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrganizCell.h"
#import "BaseManager.h"

@interface CategController_2 : UIViewController
{
        IBOutlet OrganizCell *organizCell;
        IBOutlet UITableView *organizTabl;
    BOOL visibleCell;
    int countCell;
    BaseManager* baseManager;
}

-(void)fullRefresData;
-(void)refreshData;
-(void)updateFetch:(NSString *)categName;
-(void)updateFetchWithFilter:(NSString *)filter categName:(NSString *)categName;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil filter:(NSString *)categName;
@property (nonatomic,strong) NSString * categFilter;

@property BOOL isSelected;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;

@end
