//
//  BaseManager.h
//  SpravkaMagadan
//
//  Created by gelgard on 05.05.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CHCSVParser.h"
#import "BaseInfo.h"
#import "Organization.h"
#import "BaseDelegate.h"


@interface BaseManager : NSObject  <CHCSVParserDelegate,NSFetchedResultsControllerDelegate>
{
    NSMutableArray *_lines;
    NSMutableArray *_currentLine;
    NSArray *fieldKeys;
    NSArray *fieldKeysCoord;
}

-(void)parseCSV:(NSString *)fileName;
-(BOOL) isDBEmpty;
-(BOOL)checkDatabaseVersion:(int)currentVersion;
-(int)getDatabaseVersion;
- (void) saveVersion:(int)version;
-(void)parseCSVCoordinates:(NSString *)fileName;
-(void)insertLinesCoordinatesIndb:(NSMutableArray *)lineArray;
-(NSArray *)getCategoriesForNames:(NSString*)name;
-(NSDictionary *)getAdressForStreet:(NSString*)street home:(NSString *)home;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, assign) id<BaseDelegate> delegateBase;
@property ServerFileType currentType;




- (NSFetchedResultsController *)fetchedResultsControllerCategory:(NSString* )categoryName full:(BOOL)full;
- (NSFetchedResultsController *)fetchedResultsControllerOrganization:(NSString* )categoryName name:(NSString *)name;
- (NSFetchedResultsController *)fetchedResultsControllerAdress:(NSString* )street ;
@end
