//
//  MapController.h
//  SpravkaMagadan
//
//  Created by Oleg on 27.12.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLMap/GLMap.h>
#import "BaseManager.h"
#import "CategCell.h"

@interface MapController : UIViewController
{
    
    IBOutlet CategCell *categCell;
    IBOutlet UITableView *categTabl;
    BaseManager* baseManager;
    int countCell;
    
    IBOutlet UIView *selectStreetHouse;

}

@property (nonatomic, strong) IBOutlet UILabel *menuTitle;
@property (readwrite) IBOutlet GLMapView *mapView;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) IBOutlet UILabel *selectTitle;
@property (nonatomic,strong) NSArray *coordinatesArray;

@property (readonly) NSMutableArray *pins;
@property (readonly) GLMapImageGroup *mapImageGroup;
@property (readonly) NSArray *imageIDs;

@property double longitude;
@property double latitude;
@property BOOL FromInfo;


-(IBAction)closeSelectView:(id)sender;
-(IBAction)currentLocation:(id)sender;
-(IBAction)searchByAdress:(id)sender;
-(IBAction)posToSelected:(id)sender;
- (void) loadEmbedMapWitCoordinates;
-(void)loadEmbedMap;
-(void)loadImageGroup;
@end
