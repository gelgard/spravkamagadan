//
//  TopPanel.h
//  SpravkaMagadan
//
//  Created by gelgard on 26.04.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopPanelDelegate.h"


@interface TopPanel : UIViewController
{
    NSArray *placeHolder;
    NSArray *mainTitles;
}



-(IBAction)navBtnClk:(id)sender;
-(IBAction)searchGo:(id)sender;


@property (nonatomic, strong) IBOutlet UIButton *navButton;
@property (nonatomic, strong) IBOutlet UILabel *mainTitle;
@property (nonatomic, strong) IBOutlet UITextField *searchField;
@property (nonatomic, strong) IBOutlet UIView *topForSelected;
@property (nonatomic, assign) id<TopPanelDelegate> delegate;
@end
