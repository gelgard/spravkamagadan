//
//  GLMapViewTypes.h
//  GLMapViewSDK
//
//  Copyright (c) 2014 Evgen Bodunov. All rights reserved.
//

#ifndef GLMapViewSDK_GLMapViewTypes_h
#define GLMapViewSDK_GLMapViewTypes_h

#define GLMapWorldSize (1<<30)

typedef enum GLMapAttibutionPosition
{
    GLMapAttibutionPosition_TopLeft,
    GLMapAttibutionPosition_TopCenter,
    GLMapAttibutionPosition_TopRight,

    GLMapAttibutionPosition_BottomLeft,
    GLMapAttibutionPosition_BottomCenter,
    GLMapAttibutionPosition_BottomRight,

    GLMapAttibutionPosition_Hidden
} GLMapAttibutionPosition;

typedef struct GLMapTilePos {
    union {
        unsigned long long hash;
        struct {
            unsigned int y:29;
            unsigned int x:29;
            unsigned char z:6;
        } __attribute__((packed));
    };
} GLMapTilePos;

/**
 @typedef
 
 GLMapPoint always uses internal map coordinates. From 0 to `GLMapWorldSize` for X and Y axis. Origin is at the top left corner.
 */
typedef struct GLMapPoint
{
    double x,y;
}GLMapPoint;

#define GLMapPointMake(x,y) ((GLMapPoint){(x),(y)})

extern char GLMapLogMask;

// loglevel
#define VERBOSE_FLAG	1 << 0
#define ERROR_FLAG      1 << 1
#define FATAL_FLAG		1 << 2
#define OPENGL_FLAG     1 << 3

void SendLogMessage(const char *, ...);

#define Log(LOG_FLAGS, ...) if ((LOG_FLAGS) & (GLMapLogMask)) NSLog(__VA_ARGS__)
#define LogC(LOG_FLAGS, ...) if ((LOG_FLAGS) & (GLMapLogMask)) SendLogMessage(__VA_ARGS__)

#define LogPrint(...) Log(VERBOSE_FLAG, __VA_ARGS__)
#define LogError(...) Log(ERROR_FLAG, __VA_ARGS__)
#define LogFatal(...) Log(FATAL_FLAG, __VA_ARGS__)

#define LogPrintC(...) LogC(VERBOSE_FLAG, __VA_ARGS__)
#define LogErrorC(...) LogC(ERROR_FLAG, __VA_ARGS__)
#define LogFatalC(...) LogC(FATAL_FLAG, __VA_ARGS__)

#endif
