//
//  VectorMapInfo.h
//  Galileo
//
//  Created by Evgen Bodunov on 12/6/13.
//  Copyright (c) 2014 Evgen Bodunov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef enum GLMapInfoState
{
    GLMapInfoState_Removed,
    GLMapInfoState_NotDownloaded,
    GLMapInfoState_Downloaded,
    GLMapInfoState_NeedUpdate,
    GLMapInfoState_InProgress,
}GLMapInfoState;
/**
 @abstract Notification names sent when GLMapInfo state is changed
 @discussion
 `kGLMapInfoStateChanged` is sent when GLMapInfo.state property is changed
 `kGLMapInfoDownloadProgress` is sent when GLMapInfo.downloadProgress or GLMapInfo.processedProgress property is changed
 `kGLMapInfoDownloadFinished` is sent when download is finished
 
 GLMapInfo instance is sent as an object inside notification.
*/
extern NSString *kGLMapInfoStateChanged;
extern NSString *kGLMapInfoDownloadProgress;
extern NSString *kGLMapInfoDownloadFinished;
/**
 `GLMapInfo` is a class with general information about vector map. Returned by 
 */
@interface GLMapInfo : NSObject<NSCoding>

/**
 @abstract A path to folder where offline maps is stored
 */
+ (NSString *) storagePath;

/**
 @abstract An offline map state
 @discussion
 <pre>
 typedef enum GLMapInfoState
 {
 GLMapInfoState_Removed,
 GLMapInfoState_NotDownloaded,
 GLMapInfoState_Downloaded,
 GLMapInfoState_NeedUpdate,
 GLMapInfoState_InProgress,
 }GLMapInfoState;
 </pre>
 */
@property (readonly) GLMapInfoState state;

/**
 @abstract A dictonairy with names of this map (read-only).
 @discussion Key value is a language code
 */
@property (readonly) NSDictionary *names;

/**
 @abstract Identification number of the offline map (read-only).
*/
@property (readonly) NSNumber *mapID;

/**
 @abstract The size of the offline map on the server (read-only).
 @discussion Maps are stored on the server in compresed format
 */
@property (readonly) NSNumber *serverSize;

/**
 @abstract The timestamp of the offline map on the server (read-only).
 */
@property (readonly) NSTimeInterval serverTimestamp;

/**
 @abstract The latitude and longtitude of the centre of the map (read-only).
 */
@property (readonly) double cLat, cLong;

/**
 @abstract The array contains sub-regions of the current offline map (read-only).
 @discussion Sub-regions are used for the offline maps with the large file size (approximitaly more than 300 MB)
 */
@property (readonly) NSArray *subMaps;

/**
 @abstract The path to the offline map on the disk (read-only).
 */
@property (readonly) NSString *path;

/**
 @abstract The size of the decompressed offline map on the disk (read-only).
 */
@property (readonly) NSNumber *size;

/**
 @abstract The timestamp of the offline map on the disk (read-only).
 @discussion Timestamp on the disk and timestamp on the server differs in case of a map update on the server appears.
 */
@property (readonly) NSTimeInterval timestamp;

/**
 @abstract Indicates progress of downloading of the offline map in percents (read-only).
 */
@property (readonly) float downloadProgress;

/**
 @abstract Indicates progress of decompression of the offline map on the disk in percents (read-only).
 */
@property (readonly) float processedProgress;

/**
 @abstract Return name of the country in right language.
 @param language preffered language
 @return localised name of the map. If there is no localised name for this map, native name is returned.
 */
- (NSString *) nameForLanguage:(NSString *)language;

/**
 @abstract Distance from center of the map to the given location.
 @param location Some location
 @return distance in meters
 */
- (double) distanceFrom:(CLLocation *)location;

@end
