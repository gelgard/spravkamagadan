//
//  GLMapDownloadTask.h
//  GLMap
//
//  Copyright (c) 2014 Evgen Bodunov. All rights reserved.
//

#import "GLMapInfo.h"

@class GLMapDownloadTask;
typedef void(^GLMapDownloadCompleteBlock)(GLMapDownloadTask *);

/**
 `GLMapDownloadTask` is a class for downloading offline maps.
 */
@interface GLMapDownloadTask : NSObject

/**
 @abstract Creates `GLMapDownloadTask` for `GLMapInfo` object
 @param map `GLMapInfo` with details about map
 */
-(instancetype) initWithMap:(GLMapInfo *)map;

/**
 @abstract Cancel downloading task at any time
 */
-(void) cancel;

/**
 @abstract `complectBlock` called on main thread when task is completed or cancelled
 <pre>
 typedef void(^GLMapDownloadCompleteBlock)(GLMapDownloadTask *);
 </pre>
 */
@property (copy) GLMapDownloadCompleteBlock completeBlock;

/**
 @abstract `isCancelled` set to `YES` when task was cancelled.
 */
@property (readonly) BOOL isCancelled;

@end
