//
//  GLMapImage.h
//  GLMap
//
//  Created by Evgen Bodunov on 3/16/14.
//  Copyright (c) 2014 Evgen Bodunov. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 `GLMapImage` is a bridge class, to work with GLMap internal representation of image on map.
 */
@interface GLMapImage : NSObject

/**
 @abstract Indicates whether the image object rotates with map.
 */
@property (assign) BOOL rotatesWithMap;
/**
 @abstract A boolean variable that indicates whether this image is displayed or not.
 */
@property (assign) BOOL hidden;
/**
 @abstract A position that sets image position in map internal coordinates
 */
@property (assign) GLMapPoint position;
/**
 @abstract Image offset from `position` point.
 @discussion When offset is (0,0), bottom left corner of image is displayed at `position`
 */
@property (assign) CGPoint offset;
/**
 @abstract A float variable that defines image angle
 */
@property (assign) float angle;

@end
