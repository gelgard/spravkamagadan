//
//  GLMapImageGroup.h
//  GLMap
//
//  Created by Evgen Bodunov on 3/16/14.
//  Copyright (c) 2014 Evgen Bodunov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^GLMapImageGroupFillBlock)(size_t index, uint32_t *imageID, GLMapPoint *pos);

/**
 `GLMapVectorObject` is a bridge class, to work with GLMap internal representation of image group.
 @discussion Image groups used to display small set of images at the big set of locations. E.g. 10 kinds of bookmark and 1000 bookmarks on map.
 */
@interface GLMapImageGroup : NSObject

/**
 @abstract Sets array of images to the image group
 @param images Array of `UIImage` objects
 @param complete Complete block
 @return Array of NSNumber objects with imageID's
 @discussion When images passed to `GLMapImageGroup`, they are loaded into GPU and unique object id's from GPU is returned.
 */
-(NSArray *) setImages:(NSArray *)images complete:(dispatch_block_t)complete;
/**
 @abstract Set offset for any of images in image group
 @param offset An offset
 @param imageID Image ID
 @discussion This method should be called inside or after complete block of `setImages:complete:`
 */
-(void) setImageOffset:(CGPoint)offset forImageWithID:(int) imageID;
/**
 @abstract Notify image group about changes
 @discussion Image group will call fillBlock, to get updated positions for all objects in group.
 */
-(void) setNeedsUpdate;
/**
 @abstract Set count of objects inside image group
 @param count count of objects
 @discussion Object is some location with image. Usually there is less images than objects, and multiple objects could use the same image.
 */
-(void) setObjectCount:(size_t)count;
/**
 @abstract Set object fill block
 @param fillBlock Block called for each object with `index` as input param. Block should modify `imageID` and `pos`, to set imageID and position for object at index `index`
 @discussion fillBlock is called from background thread. Add multithread syncronisation if needed.
 */
-(void) setObjectFillBlock:(GLMapImageGroupFillBlock)fillBlock;

@end
