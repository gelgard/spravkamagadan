//
//  GLMapVectorObject.h
//  GLMap
//
//  Created by Arkadi Tolkun on 3/21/14.
//  Copyright (c) 2014 Evgen Bodunov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GLMapVectorStyle;

/**
 `GLMapVectorObject` is a bridge class, to work with GLMap internal representation of vector object.
 */
@interface GLMapVectorObject : NSObject

/**
 @abstract Sets style object used for vector object.
 @param style `GLMapVectorStyle` object.
 */
-(void) setStyle:(GLMapVectorStyle *)style;

/**
 @abstract Sets style object used for vector object.
 @param multiLineData `NSArray` of the `NSData` objects.
 @discussion Creation of multiLineData with one line:
 <pre>
 NSMutableArray *multilineData = [[NSMutableArray alloc] init];
 int pointCount = 5;
 GLMapPoint *pts= (GLMapPoint *)malloc(sizeof(GLMapPoint)*pointCount);
 pts[0] = GLMapPointMake(27.7151, 53.8869); // Minsk
 pts[1] = GLMapPointMake(30.5186, 50.4339); // Kiev
 pts[2] = GLMapPointMake(21.0103, 52.2251); // Warsaw
 pts[3] = GLMapPointMake(13.4102, 52.5037); // Berlin
 pts[4] = GLMapPointMake(2.3343, 48.8505);  // Paris
 [multilineData addObject:[NSData dataWithBytesNoCopy:pts length:sizeof(GLMapPoint)*pointCount]];
 </pre>
 */
-(void) loadMultiLine:(NSArray *)multiLineData;

-(void) loadPolygon:(NSArray *)polygonData;

/* to be added
-(void) loadLine:(NSData *)lineData;

-(void) loadPolygon:(NSData *)polygonData stripIndexes:(NSData *)stripIndexes; // надо дока по формату индексов
-(void) loadMultiPolygon:(NSArray *)multiPolygonData;
-(void) loadMultiPolygon:(NSArray *)multiPolygonData stripIndexes:(NSArray *)stripIndexes;

-(void) loadRectangleWithTopLeft:(CGPoint)topLeft bottomRigth:(CGPoint)bottomRight;

-(void) loadCircle:(CGPoint)center radius:(CGFloat)radius;
-(void) loadCircle:(CGPoint)center radius:(CGFloat)radius segments:(uint16_t)segents;
*/

@end
