//
//  GLMapView.h
//  GLMapView
//
//  Copyright (c) 2014 Evgen Bodunov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class GLMapImage;
@class GLMapImageGroup;
@class GLMapVectorObject;
@class GLMapVectorStyle;

/**
 typedef description
 */

typedef void (^GLMapTapGestureBlock)(CGPoint pt);
typedef void (^GLMapBBoxChangedBlock)(GLMapPoint tl,GLMapPoint br);
typedef UIImage* (^GLMapTileErrorBlock)(GLMapTilePos pos, NSError *error);
typedef void (^GLMapCaptureFrameBlock)(UIImage *);

/**
 `GLMapView` is a custom `UIView` that renders and presents vector map.

 @warning Before calling any map position function mapView must process one layoutSubviews either manually or by system.
 */
@interface GLMapView : UIView<CLLocationManagerDelegate>

#pragma mark Rendering Control
/**
 @name Rendering Control
 */
/**
 @abstract Starts rendering timer
 @param frameInterval Renderning frame interval
 @discussion Usually called from `viewWillAppear` of `UIViewController`
 @see stopRender
 */
-(void) startRenderWithFrameInterval:(NSInteger)frameInterval;

/**
 @abstract Stops rendering timer
 @discussion Usually called from `viewDidDissappear`
 @see startRenderWithFrameInterval:
 */
-(void) stopRender;

/**
 @abstract Captures frame from `GLMapView` to `UIImage`
 @return Current state of the `GLMapView`
 @see startRenderWithFrameInterval:
 */
-(UIImage *) captureFrame;

/**
 @abstract Captures frame from `GLMapView` to `UIImage` when all load operations will finish
 @see startRenderWithFrameInterval:
 */
-(void) captureFrameWhenFinish:(GLMapCaptureFrameBlock) block;

/**
 @abstract Allows to render offscreen frames
 @discussion By default offscreen = NO. And GLMapView stop render if there is no superview. But in case you need to capture frames from standalone GLMapView, you need to set offscreen = YES; 
 */
@property BOOL offscreen;

#pragma mark User Interaction
/**
 @name User Interaction
 */

typedef enum GLMapViewGestures
{
    GLMapViewGesture_Pan      = 1,
    GLMapViewGesture_Zoom     = 1 << 1,
    GLMapViewGesture_Rotate   = 1 << 2,
    GLMapViewGesture_ZoomIn   = 1 << 3,
    GLMapViewGesture_ZoomOut  = 1 << 4,
    GLMapViewGesture_Tap      = 1 << 5,
    GLMapViewGesture_LongPress= 1 << 6,
    GLMapViewGesture_All =
        GLMapViewGesture_Pan|
        GLMapViewGesture_Zoom|
        GLMapViewGesture_Rotate|
        GLMapViewGesture_ZoomIn|
        GLMapViewGesture_ZoomOut|
        GLMapViewGesture_Tap|
        GLMapViewGesture_LongPress
} GLMapViewGestures;

/**
 @abstract `enum GLMapViewGestures` contains mask flags for all supported gestures.
 @discussion `GLMapView` supports following gestures:
 
 * `GLMapViewGesture_Pan`
 * `GLMapViewGesture_Zoom`
 * `GLMapViewGesture_Rotate`
 * `GLMapViewGesture_ZoomIn`
 * `GLMapViewGesture_ZoomOut`
 * `GLMapViewGesture_Tap`
 * `GLMapViewGesture_LongPress`
 
 and `GLMapViewGesture_All` to enable them all. Or `userInteractionEnabled = NO` to disable all gestures.
 
 Use `tapGestureBlock` and `longPressGestureBlock` to add your tap and long press handlers.
 */
@property (nonatomic, assign) GLMapViewGestures gestureMask;

/**
 @abstract `tapGestureBlock` called for each tap gesture recognized.
 */
@property (copy) GLMapTapGestureBlock tapGestureBlock;

/**
 @abstract `longPressGestureBlock` called for each long press gesture recognized.
 */
@property (copy) GLMapTapGestureBlock longPressGestureBlock;

/**
 @abstract `bboxChangedBlock` called when map is moved or zoomed.
 */
@property (copy) GLMapBBoxChangedBlock bboxChangedBlock;

/**
 @abstract `mapDidMoveBlock` called when animation or deceleration ends.
 */
@property (copy) GLMapBBoxChangedBlock mapDidMoveBlock;

/**
 @abstract `tileErrorBlock` called when empty tile without data becomes visible.
 @discussion This method is used to show custom data inside empty tiles. Or set
 */
@property (copy) GLMapTileErrorBlock tileErrorBlock;

#pragma mark User Location
/**
 @name User Location
 */

/**
 @abstract If `YES`, user location is displayed.
 */
@property (nonatomic, assign) BOOL showUserLocation;

@property (readonly) CLLocationManager *locationManager;
@property (readonly) CLLocation *lastLocation;

/**
 @abstract Set alternative user location images
 @param locationImage new user location image. If `nil`, old image is left unchanged.
 @param movementImage new user movement image. If `nil`, old image is left unchanged.
 @discussion by default location images loaded from DefaultStyle.bundle with names "userLocation.png" and "userMovement.png"
 */
- (void)setUserLocationImage:(UIImage *)locationImage movementImage:(UIImage *)movementImage;

#pragma mark Style
/**
 @name Style
 */
/**
 @abstract Loads map style
 @param styleBundle Bundle object with Style.mapcss and images
 @discussion By default GLMapView tries to load [[NSBundle mainBundle] pathForResource:@"DefaultStyle" ofType:@"bundle"]; 
 */
-(void) loadStyleFromBundle:(NSBundle *)styleBundle;
/**
 @abstract This method is used to set map languages
 @param languages Array of language codes
 @discussion Following map languags is included in vector map data for default style:
 
 * native - native language in for country or region (see `http://wiki.openstreetmap.org/wiki/Multilingual_names`)
 * en - English
 * de - German
 * es - Spanish
 * fr - French
 * it - Italian
 * ja - Japanese
 * nl - Dutch
 * ru - Russan
 * zh - Chinese
 * uk - Urkrainian
 * be - Belarusian
 
 Map codes should be ordered by priority.
 
 <pre>
 [map setLanguages:@[@"en", @"fr", @"native"]]; // check name in english, french and native languages
 </pre>
 Default value is @[@"en"]
 */
-(void) setLanguages:(NSArray *)languages;

/**
 @abstract Return set of names of map options defined in map style.
 @discussion For example in selector
 
 node|z17-[_optOn=Bar][amenity=bar],
 
 part [_optOn=Bar] requires option Bar enabled. By default all options is enabled. But you can manage enabled options via `setEnabledStyleOptions:`
 @see setEnabledStyleOptions:
 */
- (NSSet *) getStyleOptions;

/**
 @abstract Set list of enabled style options
 @param opts Set of enabled options
 @see getStyleOptions
 */
- (void) setEnabledStyleOptions:(NSSet *)opts;

#pragma mark Map position
/**
 @name Configuring the Map Position
 */

/**
 @abstract Origin point of the map coordinate system (X, Y).
 @discussion Changing the values in this property between 0 and 1 in relative coordinates describes the point of GLMapView which is considered as a center point for `mapCenter` and `setMapCenter:animated:`.
 @see mapCenter, setMapCenter:animated:
 */
@property (assign) CGPoint mapOrigin;

/**
 @abstract Coordinates of the origin point of the map.
 @see mapOrigin
 */
-(GLMapPoint) mapCenter;

/**
 @abstract Moves the map view to the given point.
 @param center The point in the internal map coordinate.
 @param animated If `YES`, the map view animates the transition to the new location. If `NO`, the map displays the new location immediately.
 @discussion
 <pre>
 // move map to Paris
 TODO
 [map setMapCenter:]
 </pre>
 @see mapCenter
 */
-(void) setMapCenter:(GLMapPoint)center animated:(BOOL)animated;

/**
 @abstract Set position of map attribution
 @param position Position to set.
 @discussion
 */
-(void) setAttributionPosition:(GLMapAttibutionPosition)position;

/**
 @abstract Defines the scrolling speed at the end of map scrolling movement.
 @param velocity Initial velocity of deceleration
 */
-(void) startDecelerate:(GLMapPoint) velocity;

/**
 @abstract Defines the current zoom level of the map view.
 @return Current zoom level of the map.
 @see setMapZoom:animated:
 */
-(double) mapZoom;

/**
 @abstract Sets the new zoom level for the map view.
 @param mapZoom Zoom level
 @param animated If `YES`, the map view animates the transition to the new zoom level.
 @see mapZoom
 */
-(void) setMapZoom:(double)mapZoom animated:(BOOL)animated;

/**
 @abstract Returns the map rotation angle.
 @return Angle in degrees from 0° to 360°
 @see setMapAngle:animated:
 */
-(double) mapAngle;

/**
 @abstract Sets the angle of the map rotation.
 @param angle Angle in degrees from 0° to 360°
 @param animated If `YES`, the change of viewing angle will be animated.
 @see mapAngle
 */
-(void) setMapAngle:(double)angle animated:(BOOL)animated;

#pragma mark Convertion functions
/**
 @name Converting Map Coordinates
 */

// convertDisplayToInternal:
/**
 @abstract Converts a point on the screen to an internal map coordinate.
 @param point The point you want to convert.
 @return The internal map coordinate of the specified point.
 @see convertInternalToDisplay:
 */
-(GLMapPoint) convertDisplayToInternal:(CGPoint)point;

/**
 @abstract Converts a distance between two points on the screen to an internal map coordinate.
 @param point The delta you want to convert.
 @return The delta in internal map coordinates.
 @see convertDisplayToInternal:
 */
-(GLMapPoint) convertDisplayDeltaToInternal:(CGPoint)point;

// convertInternalToDisplay:
/**
 @abstract Converts an internal map coordinate to a point on the screen.
 @param point The map coordinate for which you want to find the corresponding point.
 @return The point corresponding to the specified internal map coordinates.
 */
-(CGPoint) convertInternalToDisplay:(GLMapPoint)point;

// convertInternalToGeo:
/**
 @abstract Converts an internal point to geo coordinates.
 @param point The point with the specified latitude (y) and longitude (x) values.
 @return The equivalent geo coordinates of the specified internal point.
 @see convertInternalToGeo:
 */
+(GLMapPoint) convertGeoToInternal:(CGPoint)point;


// convertGeoToInternal:
/**
 @abstract Converts geo coordinates to the internal point.
 @param point The point with the specified latitude (y) and longitude (x) values.
 @return The internal point corresponding to the specified geo coordinates.
 @see convertGeoToInternal:
 */
+(CGPoint) convertInternalToGeo:(GLMapPoint)point;

#pragma mark Single image
/**
 @name Manipulating the Map
 */

/**
 @abstract Adds the `UIImage` to the map view.
 @param image The image to display
 @return The internal object, which is used to refer the image inside `GLMapView`.
 @warning You need to call `removeImage:` to release resources allocated for image inside `GLMapView`.
 @see GLMapImage, removeImage
 */
-(GLMapImage *) displayImage:(UIImage *)image;

/**
 @abstract Removes `GLMapImage` object from the map view.
 @param image The image identifier
 @see displayImage:
 */
- (void) removeImage:(GLMapImage *)image;

#pragma mark Group image functions
/**
 @name Managing Image Groups
 */

//createImageGroup:
/**
 @abstract Creates a group for storing array of `UIImage`.
 @return The image group object.
 @warning You need to call `removeImageGroup:` to release resources allocated for image group inside `GLMapView`.
 @see removeImageGroup:, setImages:complete:
 */
-(GLMapImageGroup *) createImageGroup;

// removeImageGroup:
/**
 @abstract Removes a group of images.
 @param group The ID of the group to delete
 @see createImageGroup
 */
-(void) removeImageGroup:(GLMapImageGroup *)group;

#pragma mark Vector objects functions
/**
 @abstract Creates style object.
 @param style NSString with style rules in MapCSS format
 @discussion One style could be used with many `GLMapVectorObjects`.
 */
-(GLMapVectorStyle *) createStyle:(NSString *)style;

/**
 @abstract Creates vector object
 @discussion Vector objects is used to draw any user geometry on top of the map
 */
-(GLMapVectorObject *) createVectorObject;

/**
 @abstract Removes vector object from map
 @param obj object to remove from GLMapView
 */
-(void) removeVectorObject:(GLMapVectorObject *)obj;

@end
