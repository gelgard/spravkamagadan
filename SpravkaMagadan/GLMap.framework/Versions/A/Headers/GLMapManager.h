//
//  GLMapManager.h
//  GLMap
//
//  Created by Arkadi Tolkun on 3/11/14.
//  Copyright (c) 2014 Evgen Bodunov. All rights reserved.
//

#import "GLMapDownloadTask.h"
#import "GLMapInfo.h"

#define GLMAP_FRAMEWORK_VERSION @"0.3.1"

typedef void(^GLMapListUpdateBlock)(NSArray *result, BOOL mapListUpdated, NSError *error);
/**
 A singleton object that manages offline maps
 */
@interface GLMapManager : NSObject

/**
 @return `GLMapManager` singleton object
 */
+(instancetype) manager;

/**
 @abstract API key used to download maps from server
 */
@property (strong) NSString *apiKey;

/**
 @return Returns array of `GLMapInfo` objects from `GLMapManager` cache
 */
-(NSArray *)cachedMapList;

/**
 @abstract Updates map list
 @param block complete block
 @discussion
 <pre>
 typedef void(^GLMapListUpdateBlock)(NSArray *result, BOOL mapListUpdated, NSError *error);
 </pre>
 `result` is an array of `GLMapInfo` objects
 `mapListUpdated` is YES, when updated map list is downloaded, otherwise result is equal to `cachedMapList` result.
 */
-(void) updateMapListWithCompleteBlock:(GLMapListUpdateBlock)block;

/**
 @abstract Start map download task
 @param map map object
 @param block complete block
 @return `GLMapDownloadTask` object
 @discussion
 <pre>
 typedef void(^GLMapDownloadCompleteBlock)(GLMapDownloadTask *);
 </pre>
 */
-(GLMapDownloadTask *) downloadMap:(GLMapInfo *)map withCompleteBlock:(GLMapDownloadCompleteBlock)block;

/**
 @abstract Add custom map
 @param path map path
 */
-(void) addMapWithPath:(NSString *)path;

/**
 @abstract Delete map
 @param map map to delete
 */
-(void) deleteMap:(GLMapInfo *)map;

/**
 @abstract Remove GLMap cached objects
 */
-(void) clearCaches;

/**
 @abstract Allows to download map tiles one by one
 @discussion By default tileDownloadingAllowed = NO. GLMapView checks tile in world offline map first, then in downloaded offline maps, then in downloaded earlier tiles and at last step if tile downloading is allowed it tries to download map tile.
 */
@property (assign) BOOL tileDownloadingAllowed;

@end
